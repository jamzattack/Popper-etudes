\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

global = {
  \tempo "Allegro Appassionato"
  \key g \minor
  \time 4/4
  \clef "bass"
  
  \autoPageBreaksOff
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
}

bottom = \relative c {
  \global
% bar 1
  g16-0\f( bf-2 cs-1 e-4 g-1_II bf-4 cs-1_I e-4\< \clef "tenor"
  g-1) bf( a g fs-1 g-2 a-3 g-2) |
  fs16-1( ef'-3 d-2 cs-1 c-2 a\thumb-0 fs-3_II ef-1)
  d4.\thumb-0\upbow r8 | \clef "bass" \break
  
% bar 3
  g,,16-0( bf-2 cs-1 e-4 g-1 bf-4 cs-1 e-4\< \clef "tenor"
  g-1) bf( a g fs-1 g-2 a-3 g-2) |
  fs16-1( ef'-3 d-2 cs-1 c-2 a\thumb-0 fs-3 ef-1)
  d4.\thumb-0\upbow r8\! | \clef "bass" \break

% bar 5
  f,,!16-1( af-4 b-1_III d-4 f!-1_II af-4 b-1_I d-4\< \clef "tenor"
  f!-1) af-4( g f e-1 f g f) |
  e16-1( df'-3 c-2 b-1 bf-2 g\thumb e-3_II df-1)
  c4.\thumb r8\! | \clef "bass" \break

% bar 7
  f,,16-1( af b-1 d-4 f-1 af b-1 d\< \clef "tenor"
  f-1) af-4( g f e-1 f g f) |
  e16( df'-3 c-2 b-1 bf-2 g\thumb e-3 df-1)
  c2\!\thumb | \break

% bar 9
  c16\p\<( c') d,\thumb( d'-3) ef,\thumb( ef'-3) d,\thumb( d')
  c,\thumb( c') bf,\thumb( bf') a,!\thumb( a'!) g,\thumb( g') |
  fs,16_\thumb g_\thumb gs a bf b c cs d2\f | \clef "treble" \break

% bar 11
  d16\thumb\p\<( d'-3) ef,\thumb( ef') f,( f') ef,( ef')
  d,( d') c,( c') b,( b') af,( af') |
  g,16( gs a! bf! b c cs d) ef2\f | \break

% bar 13
  ef16( ef') f,( f') g,( g') f,( f')
  ef,( ef') d,( d') c,( c') bf,( bf') |
  a,16( a') c,( c') ef,( ef') d,( d')
  c,( c') bf,( bf') a,( a') g,( g') | \break

% bar 15
  fs,16( g gs a bf b c cs) d2 |
  d8 ef!16 e f! fs g gs a2_0_\thumb | \clef "tenor" \pageBreak

% PAGE TWO

% bar 17
  d,,4_0\mf\< \clef "treble" ef''16\ff_\thumb ef d_\thumb d
  c_\thumb c bf_\thumb bf a_\thumb a g_\thumb g |
  fs16_\thumb\<( fs') ef,_\thumb( ef') d,_\thumb( d') c,!_\thumb( c'!) \clef "tenor"
  bf,\thumb( bf') a,\thumb( a') g,\thumb( g') fs,\thumb( fs'\!) | \break

% bar 19
  \new Voice {
    \stemDown
    g,16_\thumb\ff\<( bf_2) bf_2( g_\thumb)
    g( bf) bf( g) g( bf) bf( g) g( bf) bf( g\!) |
  }
  g4_\thumb
  \tuplet 3/2 {
    ef16-2\downbow\ff_>( g\thumb g'-3) g( g, ef)
    d-1_>( g\thumb g'-3) g( g, d)
    c\thumb_>( g'\thumb g'-3) g( g, c,) | \break
  }

% bar 21
  \new Voice {
    \stemDown
    g'16( bf) bf( g) g( bf) bf( g) g( bf) bf( g) g( bf) bf( g) |
  }
  g4
  \tuplet 3/2 {
    ef16-2\downbow_>( g\thumb g'-3) g( g, ef)
    d-1_>( g\thumb g'-3) g( g, d)
    c\thumb_>( g'\thumb g'-3) g( g, c,) | \break
  }

% bar 23
  \new Voice {
    \stemDown
    g'16_\thumb( a_1 bf_2 g) \clef "treble" d'_\thumb( e_1 fs_2 d)
    bf_\thumb( c_1 d_2 bf) f'_\thumb(g_1 a_2 f) |
    d16_\thumb( e_1 f!_2 d)
  }
  a'2._\thumb_0 | \clef "bass"
  \new Voice {
    \stemDown
    d,,16\f\<( f) f( d) d( f) f( d) d( f) f( d) d( f) f( d\!) | \break
  }

% bar 26
  \tuplet 3/2 {
    d16\p\<( f-1 gs b-1 c-2 cs-3 d f-1 af f d-4 cs
    c bf) gs-4( f d b-4\! b\f a gs8) r |
  }
  \new Voice {
    \stemDown
    d'16\f\<( f) f( d) d( f) f( d) d( f) f( d) d( f) f( d\!) | \break
  }

% bar 28
  \tuplet 3/2 {
    d16\p\<( e f a d-1 e f \clef "tenor" a\thumb-0 d-3
    a f-2 e-1 d\thumb-0 a\thumb-0) \clef "bass" f( e! d a\! f\f e d8) r |
  }
  \new Voice {
    \stemDown
    d'16_0_II\f\<( b'_1) b( d,) d( b') b( d,)
    d( b') b( d,) d( b') b( d,\!) | \clef "tenor" \break
  }

% bar 30
  bf'!16_1\cresc cs_1 cs bf_\thumb \clef "treble"
  cs_\thumb_( e_2) e_( cs) e_\thumb_( g_2) g_( e) g_\thumb bf_2 bf g_\thumb |
  fs16_\thumb ef'_2 ef c_\thumb c a_2 a f_\thumb
  f_( ef_2) ef_( c_\thumb) c \clef "bass" a_1 a fs_1 | \break

% bar 32
  fs2.\f <c_1 g'_1>4\ff\downbow |
  <g bf'>4 \clef "treble"
  \tuplet 3/2 { d''8_\thumb_( g_3 d) }
  bf4_2
  \new Voice {
    \stemDown \omit TupletNumber
    \tuplet 3/2 { d8_\thumb( ef_1 d) } | \clef "bass"
  }
  <g,, bf'>4 \clef "treble"
  \tuplet 3/2 { d''8_\thumb_( g_3 d) } bf4_2\< d_\thumb | \break

% bar 35
  ef4_\thumb e_\thumb f8 fs g af |
  af1_\markup{\dynamic p}_\thumb | \clef "bass"
  d,,1_0 |
  \tuplet 3/2 {
    g,16\ff-0( bf'-2 g'-4) g( bf, g,)
  }
  fs'16_1_III s
  \tuplet 3/2 {
    a16( d_2 fs,) |
    g,16( bf' g') g( bf, g,)
  }
  fs'16_1 s
  \tuplet 3/2 {
    a16( d_2 fs,) | \break

% bar 39
    g,16( bf' g') g( bf, g,)
  }
  fs'16 s
  \tuplet 3/2 {
    a16( d fs,) |
    g,16( bf' g') g( bf, g,)
  }
  fs'16 s
  \tuplet 3/2 {
    a16( d fs,) |
  }
  <g, bf'>4. r8 <g d''-2>4..
  \new Voice {
    \stemDown
    g16\upbow |
    g1\downbow |
  }
  \bar "|." \pageBreak
}

top = \relative c' {
  \global
  \repeat unfold 9 { s1 } |

% bar 10
  fs16-3( g-3 gs a bf b c cs) d2 | \clef "treble" \break

% bar 11
  s1 |
  g,16 gs a! bf! b c cs d ef2 | \break

% bar 13
  s1 | s1 |

% bar 15
  fs,16 g gs a bf b c cs d2 |
  d8^( ef!16 e f! fs g gs) a2-0-3 | \clef "treble" \pageBreak

% PAGE TWO

% bar 17
  d,,4-3^( \clef "treble" ef''16-3) ef( d-3) d(
  c-3) c( bf-3) bf( a-3) a( g-3) g( |
  s1) |

% bar 19
  \new Voice {
    \stemUp
    g,8-3[ g] g g g[ g] g g |
  }
  g4-3 s2. | \break

% bar 21
  \new Voice {
    \stemUp
    g8[ g] g g g[ g] g g |
  }
  g4 s2. | \break

% bar 23
  \new Voice {
    \stemUp
    g4 \clef "treble" d'-3 bf-3 f'!-3 |
    d4-3
  }
  a'2.->-3 | \clef "bass"
  \new Voice {
    \stemUp
    d,,8 d d[ d] d d d[ d] | \break
  }

% bar 26
  s1 |
  \new Voice {
    \stemUp
    d8 d d[ d] d d d[ d] | \break
  }

% bar 28
  s1 |
  \new Voice {
    \stemUp
    gs8-3 gs gs[ gs] gs[ gs] gs gs | \clef "tenor" \break
  }

% bar 30
  g!16-3( bf-3) bf( g-1) \clef "treble" bf-1 cs-3 cs bf
  c-1 e-3 e c-1 e-1( g-3) g( e-1) |
  ef!16-1( c'!-3) c( a-1) a( fs-3) fs( ef-1)
  ef c-3 c a-1 a( \clef "bass" fs-3) fs( ef-3) | \break

% bar 32
  ef2. e4-3\downbow |
  g4\downbow \clef "treble" \tuplet 3/2 { a8\thumb bf a } g4-3
  \new Voice { \stemUp d'4-3 } | \clef "bass"
  g,4 \clef "treble" \tuplet 3/2 { a8\thumb bf a } g4-3 d'-3 | \break

% bar 35
  ef4-3 e-3 f8 fs g af |
  af1-3 | \clef "bass"
  fs,1-3 |
  s4 ef16->-3^II( a,) s8 s4 ef'16-3->( a,) s8 | \break

% bar 29
  s4 ef'16->( a,) s8 s4 ef'16->( a,) s8 |
  g'4. r8 bf4..-3\downbow
  \new Voice {
    \stemUp
    g,,16 |
    g1 |
  }
  \bar "|." \pageBreak
% end
}


\score {
  \new Voice << \top \bottom >>
  \header {
    title = "20"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 20.pdf"
% End:
