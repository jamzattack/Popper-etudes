\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

sixteen = \relative c {
  \tempo "Allegro moderato"
  \key c \major
  \time 3/4
  \clef "bass"
  \autoPageBreaksOff

% Again, this one is invisible triplets
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 {
    
% bar 1
    c8-1^>_\markup{\dynamic {p} \italic "(capriccioso)"}( e-4) g,-0
    c e-4 g-1 a-3 e-.-1 g-. |
    a8->-0( c-2) e, a-0 c e-1 f c-.-4 e-. |
    a,8-0\<( c) fs,-3 a-.-0 c-. e-.-1 fs-. c-4 e-. | \break

% bar 4
    e8\> a,-0 c-2 c fs, a-0 a c,-.( e-.) |
    f!8-1->\p( af) c, f af c df af-4 b!-1 |
    c8-1->( df) f, af-. df!-. f-.-1 af c,-3 df-4 | \break

% bar 7
    f8( af) c,-3 df-4 f-1 g,!-4 af df e,!-1 |
    f8-1\<( af) c, df-4 f-1 g,!-0 af df-4 f, |
    b!8-2\mf \< af'-.-4( g!-.) f-1( ef'-4) d! b-1( af'-4) g\! | \break

% bar 10
    bf,,8-1\mf\< af'-.-4( g-.) e!-2( df') c bf-1( af'-4) g!\! |
    a,,!8-2\mf\< gf'-.-4( f-.) ef-1( df'-4) c a!-0( gf'-3) f\! |
    af,,8-1\mf\< gf'-.-4( f-.) d!-0( cf'-3) bf af-1( gf'-4) f-4\! | \break

% bar 13
    d!8-1\downbow\mf\< ef f-1 gf a!-2 bf f-2 gf-3 d-1 |
    ef8-2 a,!-3 bf\! f-3\> gf d!-0 ef a,! bf |
    bf'8-1\p\< cf-2 d!-1 ef-2 f-2 gf-3 d-1 ef bf-1 | \break

% bar 16
    cf8-2 f,-3 gf-4 d!-0 ef\! bf-3\> cf-4 f,-3 gf-4 |
    g!8-0\downbow\p e'!-4 cs-1 bf'-3 g!-2 e'!-4 cs-1 bf'-3 g-1 |
    fs,,8-2\upbow ef'-4 c-1 a'!-4 fs-2 ef'-4 c! a'!-3-0 fs-1 | \break

% bar 19
    f,,!8-2 d'! b!-1 af'-3 f!-2 d' b-1 af'-4 f!-1 |
    e,,!8-2 df'-4 bf-1 g'-3 e!-2 df'-4 bf-1 g'-4 e!-1 |
    b!8-3\< e-1 \clef "tenor" g-1 b!-3 e-3-0 d-2 b\thumb g-2 e\thumb | \break

% bar 22
    c8-2->_III\upbow\f e\thumb_"II"
    e'-3 b,-1->_III e\thumb ds'-2 bf,-1-> e\thumb d'-2 |
    a,8-1\downbow\< e'-1 d'-3 cs a\thumb f!-2 e-1 cs-3_III a |
    bf8-2->\f d\thumb d'-3 a,-1-> d\thumb cs'-2 af,-1-> d\thumb c'-2 | \break

% bar 25
    g,8\thumb\downbow\< d'\thumb-0 c'-2 b!-1 g-3 ef-1 d b!-2 g\thumb-0\! |
    af8-1\upbow\f\< d\thumb d'-3 c af-4_II fs-2 d\thumb c-3_III a |
    g8\thumb d'\thumb c'-2 b!-1 g-3 ef-1 d\thumb b!-2 g\thumb-0 | \break

% bar 28
    af8-1 d\thumb-0 d'-3 c af-4 fs-2 d\thumb c-3 a\! |
    g8\thumb\downbow\f b-2 d,-1_IV g_"III" b-1 d-0-2 \clef "treble"
    g\thumb-0 b-0-1 d-0-2 |
  }
  g2.-0_3\fermata | \clef "bass" \break

  \tuplet 3/2 {
% bar 31
    e,,8->\p( g) b, e g b c g b |
    c8->-1\<( ef) g, c ef g-3 af fs-2 g-3\! |
    fs8-2\mf( a-0-2) c,-3_II f-1_I( af-4) c,-3_II ef-1( g) c,-4 | \pageBreak

% PAGE TWO

% bar 34
    df8-1->\>( f) af,-2 c-1->( ef-4) a,-4 bf-1->( d-4) f,-3 |
    ef8-1\p( g-4) c, b!-1->( d) g,-0 c-1->( e) g,-0 |
    cs8-1->\p\>( e!) g,-0 d'-1->( f) g,-0 b-1->( d) g,-0 | \break

% bar 37
    c!8-1->\p( e) g, c e g a-3 e-1 g |
    a8-0->( c-2) e, a-0 c e-1 f c-.-4 e-. |
    a,8-0->\<( c) fs,-3 a-.-0 c-. e-.-1 fs-. c-.-4 e-. | \break

% bar 40
    e8\> a,-0 c-2 c fs, a-0 a c,-.( e-.) |
    f!8-1->\p( af) c,-3 f-1 af c-1 df af-4 b!-1 |
    c8-1->( df) f, af-. df-. f-.-1 af c,-3_II df-4 | \break

% bar 43
    f8( af) c,-3_II df-4 f-1 g,!-3 af df-2 e,!-1 |
    f8-1\<( af) c, df-4 f-1 g,!-0 af df-4 f,\! |
    b8-2\mf\< af'-.-4( g!-.) f-1( ef'-4) d! b!-1( af'-4) g\! | \break

% bar 46
    bf,,8-1\mf\< af'-.-4( g-.) e!-2( df') c bf-1( af'-4) g!\! |
    a,,!8-2\mf\< gf'-.-4( f-.) ef-1( df'-4) c a!-0( gf'-3) f\! |
    af,,8-1\mf\< gf'-.-4( f-.) d!-0( cf'-3) bf af-1( gf'-4) f-4\! | \break

% bar 49
    d!8-1\downbow\mf\< ef f-1 gf a!-2 bf f-2 gf-3 d-1 |
    ef8-2 a,!-3 bf\! f-3\> gf d!-0 ef a,! bf |
    bf'8-1\p\< cf-2 d!-1 ef-2 f-2 gf-3 d-1 ef bf-1 | \break

% bar 52
    cf8-2 f,-3 gf-4 d!-0 ef\! bf-3\> cf-4 f,-3 gf-4 |
    g!8\p\< df''-4 bf-1 g-4 e!-1 df-4 c-4 ef'-4 c |
    b,!8-2 f''-4 d!-1 b!-4 af-1 f-4_III e!-3 g'-4 e! | \break

% bar 55
    ds,8-2_III fs'-3 ds-1 c-4 a-1 fs-4_III f-3 af'-4 f-1 |
    e8-1\f f fs g-1 gs-2 a-2 bf a g-1 |
    f8-2 e d-4 cs bf a-0 g f-.( e-.) | \break

% bar 58
    d8->\mf( f) a, d f a-0 bf-1 f a |
    bf8->( d) f, a-0 bf d-1 f a,-3 bf-4 |
    ds,8-1->( fs-4) a,-2 ds fs-4 a-0 c-3 fs,-4 a-0 | \break

% bar 61
    c8-1->\<( ef) fs,-2 a-0 c-1 ef-1 fs-3 c-4 e-1 |
    g8-4\f fs f e! d c b\prall( a) g |
    c8 b a g fs f e\prall( d) c | \break

% bar 64
    f8\prall( e) d a''-0_3 g-4 f e d-4 c |
    b8\prall( a) g f d b g f d |
    c8->\mf( e'-3) g, c-1 e g a-3 e-1 g | \break

% bar 67
    c8-1->( e) g, c e-1 g a-0_3 e-1 g-4 |
    g8 c,-4 e e g,-4\< c c e,-1 g |
    g8 c, e e g, c c e, g |
  }
  <c, e'>2.\f\fermata \bar "|." \pageBreak
}

\score {
  \new Staff \sixteen
  \header {
    title = "16"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B sixteen.pdf"
% End:
