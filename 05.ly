\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

five = \relative c' {
  \tempo "Allegro non troppo"
  \key a \major
  \time 6/8
  \clef "bass"

% bar 1
  e8.-1\downbow_\markup{\dynamic "mf" \italic "scherzando"} fs16 a,8-0 b8.-1 cs16 e,8 |
  fs8.-3 a16-0 cs8-1 e8.-4 d16 a'8-2-0 | \clef "tenor"
  gs8.-1 b16 fs8\thumb e8.-3 gs16-1 d8 |
  cs8.-1 e16 b8\thumb a8.-3_G cs16-1 gs8-2 |
  fs8.-1-> a16-3 cs8-1 fs8.\thumb gs16 a8 | \break

% bar 6
  as8.-2 g16-1 fs8\thumb e8. d16 cs8 |
  b8.\thumb d16-0\thumb fs8-2 b8.-1 cs16 d8-3 | \clef "treble"
  e8.-0-3 d16-2 cs8-1 b8.\thumb a16 g8 |
  fs8.-1 fs'16-4 fs,8 e8.\thumb e'16-3 e,8 |
  d8.\thumb d'16-3 d,8 cs8.\thumb cs'16-3 cs,8 | \break

% bar 11
  d8.\cresc\thumb d'16-3 d,8 cs8.\thumb cs'16 cs,8 |
  b8.\thumb b'16 b,8 a8.\thumb a'16 a,8 | \clef "tenor"
  b8.\thumb b'16 b,8 a8.\thumb a'16 a,8 |
  gs8.\thumb gs'16 gs,8 fs8.\thumb fs'16 fs,8 |
  cs8.\f-1 fs16\thumb a8-2 cs8.\thumb fs16-3 cs8 | \break

% bar 16
  d8.-1 cs16 ds8-1 cs8. es16-2 cs8 |
  fs8.-3 cs16\thumb a8 \clef "bass" fs8.\thumb cs16-1 a8-3 |
  fs4.-1~ fs8 r r |
  d'8.\mf\downbow b'!16 a8 g'8.-4-> fs16 a,8-0 |
  fs'8.-4-> f16-3 gs,8-1 f'8.-4-> e16-3 g,8-1 | \break

% bar 21
  fs8.\>-1 e'16-4 ds8 f,8. ef'16-4 d8 |
  e,8.-1 d'16-4 cs!8 ds,8.-1 c'16-3 b8 |
  e,8.\mf-> c'16-2 b8 a'8.-3-0 g16-2 b,8 |
  g'8.-3 fs16-2 as,8-1 fs'8.-3 f16-2 a,8-1 |
  gs8.-1-> f'16-3 e8 g,8.-1-> e'16-3 ef8-2 | \break

% bar 26
  fs,8.-1->\> ef'16-3 d8 es,8.-1-> d'16-3 cs8 |
  fs,8.-2\p\< d'16 cs8 a8.-0 fs16 cs8-4 |
  es8.-> d'16 cs8 gs8.-4 es16 cs8 |
  fs8.-2\!\< d'16 cs8 a8.-0 fs16 cs8-4 |
  es8.-> d'16 cs8 gs8.-4 es16 cs8 | \break

% bar 31
  fs8.-2\mf\< d'16 cs8 a8.-0 gs'16-4 fs8 |
  fs,8.-2 d'16-3 cs8 cs,8.-1 b'16-4 a8-2 |
  fs,2.\f~ |
  fs4 r8 r4 r8 |
  b8.-3_>\downbow d'16 b8 bf,8.-2_> d'16-4 bf8-1 | \break

% bar 36
  a,8.-1_> d'16-4 a8-0 as,8.-2_> d'16-4 as8-1 |
  b,8.-3_> d'16 b8 bf,8.-2_> d'16-4 bf8-1 |
  a,8.-1_> d'16-4 a8-0 as,8.-2_> d'16-4 as8-1 |
  b,8._> d'16 b8 cs,8.-3_> e'16-4 cs8 |
  d,8.-2_> fs'16-4 d8 e,8._> g'16-4 e8 | \break
  
% bar 41
  fs8.-2 gs16 es8 fs8. fs,16-4_G f8-3 |
  e8.-2 fs'16-4 ds8 e8. e,16-4_G ds8-3 |
  d!8. e'16-4 cs8 d!8. d,16-4 cs8 |
  c8. d'16-4 b8 c8. c,16-4 b8 |
  as8.\mf-2 g'16 fs8 b,8. d'16 b8 | \break

% bar 46
  cs,8.-1 b'16 as8 d,8.-2 fs'16-4 d8 |
  e,8.-3_G g'16-4 e8 es,8.-3_G gs'16-4 es8-1 |
  fs8.-2 cs16-4 as8-1 fs4-4 r8 |
  fs'8.\p-2 gs16 es8 fs8. as,16-1 cs8 |
  f8.-2 g16 e8 f8. a,16-1 c8 | \pageBreak

% PAGE TWO

% bar 51
  e8.-2 fs!16 ds8 e8. gs,16-1 b8 |
  e8. fs16 ds8 e8. gs,16-1 b8 |
  e8.\> f16 gs,8 e'8. f16 gs,8 |
  e'8. f16 gs,8 e'8. f16 gs,8 |
  e'8.\p-2 fs16 a,8-0 b8.-1-> cs16 e,8 | \break

% bar 56
  fs8.-3 a16-0 cs8-1 e8.-4 d16 a'8-0-2 | \clef "tenor"
  gs8.-1 b16 fs8\thumb e8.-3 gs16-1 d8-2 |
  cs8.-1 e16 b8\thumb a8.-3 cs16 gs8-2 |
  fs8.-1\< as16 cs8-1 fs8.\thumb gs16 as8 |
  b8.-3 fs16 d8 b8.\thumb cs16 d8 | \break

% bar 61
  a8.-1_G cs16-3 e8-1 a8.\thumb b16 cs8 |
  d8.-3 a16 fs8 d8.\thumb e16 fs8 |
  cs8.-1_G\< es16-3 gs8-1 \clef "treble" cs8.\thumb ds16-1 es8-2 |
  fs8. cs16a8-2 fs8.\thumb gs16 a8 |
  cs,8.-1_G es16-3 gs8-1 cs8.\thumb ds16 es8-2 | \break

% bar 66
  fs8.\f-3 cs16\thumb as8-2 f'8.-1 c16\thumb a8-2 |
  e'8.-3 b16\thumb gs8-2 ef'8. bf16\thumb g8-2 |
  fs8.-2\> a16\thumb d8-3 cs8.-3 gs16\thumb es8-2 |
  e!8.-2 g16\thumb c8-3 b8.-3 fs16\thumb ds8 |
  bf'8.\p-3 f16\thumb d8 a'8.-3 e16\thumb cs8-2 | \break

% bar 71
  \clef "tenor" af'8.-3 ef16\thumb c8-2 g'8.-3 d16\thumb b8-2 |
  a8.-2\< cs16\thumb fs8-3 f8.-3 c16\thumb as8-2 |
  gs8.-2\< b16\thumb e8-3 ef8.-3 bf16\thumb g8-2 | \clef "bass"
  fs8.\f-2 d16-0 a'8-0 fs8. d'16-3 a8-0 |
  fs'8.-3 a,16-0 d8-2 e8.-4 a,16-0 cs8-1 | \break

% bar 76
  d8.-2 d,16-0 a'8-0 fs8.-1 d'16-2 a8-0 |
  fs'8.-3 a,16-0 d8-2 e8.-4 a,16 cs8-1 |
  d8.\cresc d,16-0 a'8-0 f8.-2 d'16-4 a8 |
  f'8.-2 a,16-0 d8-1 f8.-4 a,16-0 d8 |
  f8.-4 a,16-0 ef'8-2 f8. a,16 ef'8 | \break

% bar 81
  f8.-4 a,16-0 ef'8-2 f8. a,16 ef'8 |
  f8.\f-4 bf,16 d8 f,8.-2 bf16 d,8 |
  f8.-2 bf,16 d8-0 f,8.-4 bf16-2 d,8 |
  g''8.-4 bf,16-2 ef8-1 g,8.-1 bf16 ef,8-4 |
  g8.-1 bf,16-2 ef8-1 g,8.-0 bf16 ef,8 | \break
  
% bar 86
  g''8.-4 bf,16-2 e!8-1 g,8.-1 bf16 e,!8-4 |
  g8.-1 bf,16-2 e!8-1 g,8.-0 bf16-2 e,!8 |
  f8.\f\< a16 c8 f8. a16 c8 |
  df8.-3->\! bf16-1\> g8-4 e8.-1 bf16 g8 | \break

% bar 90
  f8.\< a16 c8 f8. a16 c8 |
  df8.->\! bf16\> g8 e8.-1 bf16 g8 |
  f8.\< a16 c8 f8. a16-0 c8-1 |
  ef8.-4\< a,16-0 c8-1 ef8. a,16 c8 | \break

% bar 94
  ds8.-4 a16-0 c8-1 ds8. a16 c8 |
  e8.-1\f\< fs16 a,8 b8.-2 cs16-4 ds,8-1 |
  e8.-2 a16 cs8-1 e8.-4->\ff d16-2 gs,8-3 |
  e'8.-1\< fs16 a,8-0 b8.-2 cs16-4 ds,8-1 |
  e8.-2 a16-0 cs8-1 e8.-4->\ff d16-2 gs,8-3 | \break

% bar 99
  e'8.-1\< fs16 a,8 b8.-2 cs16-4 ds,8-1 |
  e8.-1 fs16 a,8-1 b8.-2 cs16-4 e,8-2 |
  a8.-1\! cs16-4 e8 a8.-0 cs16-1 e8 | \clef "treble"
  a8.\thumb cs16-1 e8-2 a8.-0\thumb cs16^0_4 e8^0_2 |
  a2.^0_3\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \five
  \header {
    title = "5"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 05.pdf"
% End:
