\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

four = \relative c {
  \tempo "Andante con moto"
  \key fs \major
  \time 3/4
  \clef "bass"
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 { % This one is also mostly triplets
% bar 1
    cs8\p-1( as'-2 fs\thumb ds-2 b'-3 fs\thumb b,\thumb gs'-1 fs |
    cs8-1 as' fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs) |
    cs8-1( as' fs\thumb cs'\thumb as-2 fs'-3 cs \clef "tenor" as'-2 fs\thumb |
    cs'8-4 b-3 gs-1 b as fs\thumb as gs es-3) | \break

% bar 5
    cs8-1( as'-2 fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs |
    cs8 as' fs ds b' fs b, gs' fs) |
    cs8-1( as'-2 fs\thumb \clef "treble" cs'-2 as\thumb fs'-3 cs\thumb as'-2 fs\thumb |
    cs'8-4 a-2 fs\thumb a-2 fs cs\thumb fs-3 cs a-2) | \break

% bar 9
    fs'8-3( d-1 a-2 d-1 a fs\thumb a-2 fs d\thumb) |
    d'8-3( bf-1 g bf g d\thumb g d bf\thumb) |
    bf'8-3( g-1 ef-3 g ef bf\thumb ef bf g\thumb) | \break

% bar 12
    ef8-2( g\thumb bf-2 ef-1 g-3 d\thumb f-2 ef-1 bf-2 |
    g8) bf\thumb( ef-3 g-1 bf-3 f\thumb a-2 g-1 ef-3) |
    bf8( ef\thumb g-2 bf\thumb ef-3 c-1 d-2 c-1 g-2 | \break

% bar 15
    af8-3) g-2( c-1 bf\thumb g-2 ef\thumb c-2_G bf-1 g'-2) |
    g8( f ef'-3 d-2 c af-3 g-2 f af-3 |
    ef8-2) d-1( c'-3 bf-2 af-1 f-3 ef d f-3 | \break

% bar 18
    c8-3) bf-1( g'-2 f-2 d\thumb \clef "bass" bf-2 af-1 f-2 d-0 |
    bf8-3_G) a( af f'-4 d-1 bf-2 af f-4 d-1) |
    ef8-2( g-0 bf ef-1 g d f ef bf | \break

% bar 21
    g8-0) bf-2( ef g-1 bf-4 f-1 af-2 g ef-4) |
    g8-1( ef-4 g-1 c-1_D ef d c g-2_G ef\thumb) |
    cf8-2_C( ef_"G"\thumb af-3 cf-1_D ef-3 df-2 cf-1 af-3 ef\thumb) | \break

% bar 24
    bf8-1( ef\thumb gf-2 bf\thumb ef-3 cf-1 bf gf-2 ef\thumb) |
    a,8-1( ef'\thumb gf-2 c-1 ef d c g-2 ef\thumb) |
    a,8-1_C( fs-4 f-3 e-2 ds-1 d-2 cs! c-0 a'-1_G) |
  } % end triplets
  a2\>( as4-2~ | \break

% bar 28
  as4_G b!-3 bs-4) |
  \tuplet 3/2 { % back to triplets
    cs!8\p-1( as'-2 fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs |
    cs8 as' fs ds b' fs b, gs' fs) |
    cs8( as' fs cs'\thumb as-2 fs'-3 cs \clef "tenor" as'-2 fs\thumb | \break

% bar 32
    cs'8-4 b-3 gs-1 b-3 as fs\thumb as gs es-3) |
    cs8-1( as'-2 fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs |
    cs8-1 as'-2 fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs) |
    cs8( as'-2 fs\thumb \clef "treble" cs'-2 as\thumb fs'-3 cs\thumb as'-2 fs\thumb) | \break

% bar 36
    cs'8-4( a-2 fs\thumb a-2 fs cs\thumb fs cs a-2) |
    fs'8-3( d-1 a-2 d a fs\thumb a fs d\thumb) |
    d'8-3( bf-1 g-3 bf-1 g d\thumb g d bf\thumb) | \pageBreak

% PAGE TWO

% bar 39
    bf'8-3( g ef g ef bf ef bf g\thumb) |
    ef8-2( g\thumb bf-2 ef-1 g-3 d\thumb f-2 ef-1 bf-2 |
    g8) bf\thumb( ef-3 g-1 bf-3 f\thumb a-2 g-1 ef-3) | \break

% bar 42
    bf8( ef\thumb g-2 bf\thumb ef-3 c-1 d-2 c-1 g-2 |
    af8-3) g-2( c-1 bf\thumb g-2 ef\thumb c-2_G bf-1 g'-2) |
    g8-2( f-1 ef'-3 d-2 c af-3 g-2 f af-3 | \break

% bar 45
    ef8-2) d-1( c'-3 bf af f ef d f-3 |
    c8-2) bf-1( g'-2 f-2 d\thumb \clef "bass" bf-2 af-1 f-2 d-0 |
    bf8-3_G) a( af f'-4 d-1 bf-2 af-1 f-4 d-1) | \break
    
% bar 48
    ef8\mf-2( g-0 bf-2 ef-1 g d f ef bf) |
    g8-0( bf-2 ef g-1 bf f-1 a-2 g ef-4) |
    c8-1( ef-4 g-1 c-1_D ef-3 d-2 c-1 g-2_G ef\thumb) | \break

% bar 51
    cf8-2( ef\thumb af-3 cf-1 ef-3 df-2 cf-1 af-3 ef\thumb) |
    bf8-1( ef\thumb gf-2 bf\thumb ef-3 cf-1 bf gf ef) |
    a,8-1( ef'\thumb gf-2 c-1 ef d c gf ef) | \break

% bar 54
    a,8-1\<( fs!-4 f-3 e-2 ds-1 d-2 cs! c-0 a'-1) |
  } % end triplets
  a2-1\>( as4-2~ |
  as4 b!-3 bs-4) |
  \tuplet 3/2 { % back to triplets
    cs!8\p-1( as'!-2 fs!\thumb ds!-2 b'! fs b,!\thumb gs' fs) | \break

% bar 58
    cs!8-1( as'!-2 fs!\thumb ds!-2 b'! fs b,!\thumb gs' fs) |
    cs8( as' fs\thumb cs'\thumb as-2 fs'-3 cs \clef "tenor" as'-2 fs\thumb) |
    cs'8-4( b-3 gs b as fs\thumb as-2 gs-1 es-3) | \break

% bar 61
    cs8-1( as'-2 fs\thumb ds-2 b'-3 fs b,\thumb gs'-1 fs) |
    cs8( as' fs ds b' fs b, gs' fs) |
    cs( as' fs cs'-2 as\thumb ds cs as fs-2) | \break

% bar 64
    ds8\thumb( as'\thumb fs-2 ds'-3 as es'-4 ds as fs-2) |
    ds8\thumb( b'-1 fs-2 ds'-3 b-1 es-4 ds-3 b-1 fs-2) |
    \clef "treble" es8\thumb( cs'-1 gs-2 es'-3 cs-1 fs-4 es-3 cs-1 gs-2) |
    fs8\thumb( cs'\thumb as fs'-3 cs gs'-4 fs cs as-2) | \break

% bar 68
    fs8( d'-1 a-2 fs'-3 d g-4 fs d a) |
    fs8\thumb( cs'\thumb as-2 fs'-3 cs gs'-4 fs cs as) |
    fs8( cs' as fs' cs gs' fs cs as) |
  } % end triplets
  fs2.\thumb~ |
  fs4 r\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \four
  \header {
    title = "4"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 04.pdf"
% End:
