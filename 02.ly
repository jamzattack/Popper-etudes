\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

two = \relative c {
  \tempo "Andante"
  \key g \major
  \time 4/4
  \clef "bass"

  g16-0\p( d' b'-1 a c b d b
  a g b g e d g d) |
  g,16( d' b' a c b d b
  a g b g e d g d) | \break
  
% bar 3
  a16-1( e' c'-2 b d-2 c e-4 c-1
  b-1 a c a-0 f-2 e a e) |
  a,16( fs'-3 c' b d c g'-4 fs
  e ef-4 d-3 cs-2 c-1 a-0 fs-3 d) | \break

% bar 5
  g,16( d' bf'-1 a c bf d-1 bf
  a-3 g bf g ef-1 d g d) |
  g,16( ef'-1 bf'-1 a-0 c-3 bf g'-4 bf,-2
  a!-3 g ef'-2 g, f-3 ef-1 cs'-4 ef,) | \break

% bar 7
  d16( b'!-1 d!-4 b'-3 a-2 g-1 cs,-1 d
  ds-3 fs-3 e-1 as,-2 b-3 d-0-3 c-4 e,-1 |
  g16-2) fs( e'-4 ef-3 d-2 cs-1 c-2 b
  d-4 cs c a fs d c a) | \bar "||" \break
  
% bar 9
  g16-0( ef'-1 bf'-1 a c-3 bf \clef "treble" c'-1 bf\thumb
  ef-3) bf\upbow( af-3 bf af g f-1 g |
  f16-1) ef\thumb( \clef "bass" g,,-0\downbow ef'-1 c'-2 b d c
  \clef "treble" c'-1) g-2( f-1 g-2 f ef\thumb bf-1_G b-2) | \break

% bar 11
  c16-3_G( \clef "bass" af,-1 g-0 ff'-2_D ef df'-4 c-3 ef,-1_G)
  gf-4( bf,-2 a-1 gf'-3_D f-2 df'-4 b-2 c-3) |
  df16-4( bf,-2_G a-1 gf'-3_D f-2 ef'-3 df-2 f,-1_G)
  af-3( c,-2 b-1 af'-3_D g-2 ef'-3_A cs-1 d-2) | \break

% bar 13
  ef16-3\<( f-2 fs-3 g_4 fs_4 ef_1 c_1 a!_0
  gs-1) a( af b bf-4 g-1 e-4 cs-1) |
  c16_1( cs_2 d_3 ef_4 d_4 b_1 af_4 f_1)
  e_1( f fs g cs,_1 d ds e) | \break

% bar 15
  d16-1\mf( g-0 b-3 d-0 fs!-1 g e'-4 d)
  fss,-1\upbow\<( gs-2 f'-4 e gss,-1 as-2 g'-4 fs |
  f16-2) e-1\>( c-4 a-1 f-2 e-1 c a
  g-2) fs-1( e'-4 ef e,-3 d b'-3 a) | \pageBreak

% PAGE TWO
% bar 17
  g16\!( bf-1 cs e g->-4 fs ef-1 c)
  g( bf cs e g-> fs ef c) |
  g16( b! d fs-3 g-4 b d b)
  g'2_4\upbow | \bar "||" \break

% bar 19
  ef,,16\f-1( bf'-1 g'-3 f af g f g
  f) ef-4( c ef c bf-2 g-0 ef) |
  c16( g' ef'-2 d f-4 ef d ef
  d) c-4( af c af g-0 ef-2 c) | \break

% bar 21
  cs16-1( bf'-3_G a af g-0 e'-4 ef-3 d-2
  cs) bf'-3_D( a-2 g-1 f'8-3->_A e!-2) |
  d,,16-2(b'-4_G as a gs f'-3_D e ef
  d-0) b'-3( as-2 gs fs'!8-.->_A es-2) | \break

% bar 23
  gs16\f-4( fs es fs a,-0 b-1 bs-2 c-1
  e) d( a fs-3 e8 d-0) |
  g'!16\p-4( f! e! f a,-0 bf-1 b-2 c-1
  ef-4) df( af f ef8-4 df) | \break

% bar 25
  c16\p\<-1( bf'-4 g-1 f'-4 e!-3 g, b c,)
  cs-2->( bf'-4 g-1 f' e!-3 g, b c,) |
  d16-2->( b'!-4 g-1 fs'-4 e g, b-4 d,-2)
  e16-1->_G( d'-4_D cs-3 a'-4 g-2 b,-1 c-3 e,-1) | \break

% bar 27
  d16\f-2( e cs d c-2 d b c
  b-2\>) c( as b a-1 e'-4 d fs,-1) |
  g16\p-0( d'-0 b'-1 a c-2 b d b
  a g b g e d g d) | \break
  
% bar 29
  g,16( b'-3 f'-2 e g-3 f af-4 f
  e!-3 d f d c-2 b d b) |
  g,16_0( g'-1 e'-3 d f-2 e g e
  d-2 c e-4 c a-0 g-4 e c) | \break

% bar 31
  g16-0( fs'!-3 c' b d cs c b
  a g fs e d c b a) |
  g16\>( d' b' a c b d b)
  g,( d' b' a c b d b) |
  <g, g'>1\pp \bar "|." \pageBreak
% end
}

\score {
  \new Staff \two
  \header {
    title = "2"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 02.pdf"
% End:
