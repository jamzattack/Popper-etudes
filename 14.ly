\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

fourteen = \relative c' {
  \tempo "Moderato"
  \key d \major
  \time 4/4
  \clef "tenor"

% bar 1
  d16\mf_"I"\downbow( d'-4) cs\upbow( b a_0\thumb gs-3_II fs e
  d_0\thumb cs-3_III b a g_0\thumb fs-3_IV e-2 d-1 |
  e16\< fs g\thumb a-1 b cs d\thumb e-1
  fs-2 g-3 a\thumb b-1\! a8) r | \break

% bar 3
  e16-1_I( e'-0-3) d-3( cs-2 b a\thumb-0 g-3_II fs
  e d\thumb-0 cs-3_III b a g\thumb fs-3_IV e |
  fs16\< g\thumb a b cs d\thumb e fs
  g a\thumb b cs\! b8) r | \clef "treble" \break

% bar 5
  f16-3_I( fs'-3) e( d cs-2 b a\thumb-0 g-3_II
  fs e d\thumb-0 cs-3_III \clef "tenor" b a g\thumb-0 fs-3_IV |
  g16\thumb-0 a b cs d_0\thumb e fs g
  a_0_I\thumb b cs d cs4-2) | \clef "treble" \break

% bar 7
  d16-1\downbow( d'-3) cs-2\upbow( b-1 a-2 g fs-2 e
  d-3 cs b-1 a\thumb g-3_II fs e d\thumb | \clef "bass"
  cs16-3_III b a g\thumb fs-3_IV e-2 d-1 e
  fs g\thumb_"III" a-1 b \clef "tenor" cs-3 d\thumb_"II" e-1 fs-2 | \break

% bar 9
  g16-3 a\thumb_"I" b-1 cs-2 d-3 a\thumb b cs
  d a b cs d a b cs) | \clef "treble"
  d2.\downbow r4 |
  b16\downbow( cs) d\upbow( cs b cs b g-3_II
  fs-2 g fs d\thumb-0 b-2_III d_"II" fs-2 b-1_I) | \break

% bar 12
  a16\thumb-0\downbow( b cs b a b a gs-3_II
  fs g fs cs-3_III) a4-1\downbow | \clef "tenor"
  g'!16\thumb_"I"( a-1) b\upbow( a g a g fs-3_II
  e fs e b_"III" g-1 b e-2 g\thumb) | \break

% bar 14
  fs16\thumb\downbow( g-1 a-2 g fs g fs e-3_II
  d-2 e d a-3_III) fs4_1\upbow |
  e16\thumb_"III"\downbow( fs g a b\thumb cs ds e
  fs\thumb g a b-1) c8-2->\upbow( b) | \break

% bar 16
  cs16\downbow( d) b\upbow( cs a\thumb b g-3 a
  fs-2 g-3 e fs d8\thumb) r |
  d,16-0\downbow( e-1 fs-3 g-4 a-0 b cs d
  e-1 fs g-1 a \clef "treble" b-1 cs d-3 cs | \break

% bar 18
  e16) d-3\upbow( b a\thumb fs-2_II e-1 d\thumb b-2_III
  a4.-1) r8 | \clef "tenor"
  fs16-3_II\downbow( g a-0_I b cs d e-1 fs-3 \clef "treble"
  g-1 a-2 b-1 cs-2 d-3 e-1 fs e | \pageBreak

% PAGE TWO

% bar 20
  g16-3) fs-2\upbow( a-4 fs d\thumb b-2_II a fs-3
  d4.-3-0) r8 | \clef "tenor"
  b16-1_I\downbow( cs d e-1 fs gs-1 as-2 b \clef "treble"
  cs-1 d-2 e-1 fs-2 gs-1 as-2 b-3 as-2\upbow)( | \break

% bar 22
  cs16-0) \clef "tenor" \harmonicsOn
  b,,\thumb-0\downbow( d\thumb-0) cs\thumb-0\upbow( e\thumb-0)
  d\thumb-0\downbow( fs\thumb-0) e\thumb-0\upbow( g4.\thumb-0)
  \harmonicsOff r8 |
  fs16\f\thumb\downbow_"I"( \clef "treble" fs'-3) e-2( d-1 cs-2 b-1 a\thumb-0 g-3_II
  fs-2 g fs g fs e d\thumb-0 cs-3_III) |
  b8\downbow d16\thumb\upbow( fs b-1 cs d cs
  b4.) r8 | \clef "bass" \break

% bar 25
  f,16\mf_2\downbow( a_0) d_4\upbow( c b! c a f'
  g f d c bf a g f |
  d16) e\downbow( f a-0 \clef "tenor"
  d-1 e-3 f-4 a_0\thumb) d2-3\upbow | \break

% bar 27
  bf16-1\downbow( c) d\upbow( c-2 bf-2 a g\thumb f-3_II
  e f e d c\thumb bf-3_III a g |
  f16\thumb) a-2\downbow( c\thumb_"II" e-2 \clef "treble"
  f\thumb a-2 c\thumb_"I" e-2) f2\upbow | \break

% bar 29
  d16-1\downbow( e!) f\upbow( d-3 b!-1 gs-3_II f-2 d\thumb-0 \clef "bass"
  b-4 gs f-2 d b-4_III gs f-4_I d) |
  a'16\downbow_"III" b cs d_"II" e-1 f-2 gs-4 a-0_I
  b-1 cs-3 d e-1 fs-2 gs-4 a8-0_2 | \clef "tenor" \break

% bar 31
  b16-3\downbow( a) b\upbow( a b a
  b a b a b a b a b a |
  b16 a g-1 fs-3 e d-4 cs b \clef "bass"
  a-0 g-4 fs e d-0 cs-4 b a | \break

% bar 33
  d16 e fs g a-0_I b cs d \clef "tenor"
  e-1 fs g-1 a b-1 cs d8-3) |
  d4.\downbow c16\upbow( b
  a-0\thumb g-3_II fs-2 e-1 d-0\thumb c-3_III b-2 a-1 | \break

% bar 35
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 {
    g8-0\thumb) b-1_III\downbow( d-2 \clef "treble"
    g-0\thumb) b-1-0 d-2-0
  }
  g2-3-2\upbow | \clef "bass"
  g,8._4_I\downbow fs16\upbow( e d-4 cs! b
  a-0 g-4 fs e d-0 cs!-4 b a | \break

% bar 37
  \tuplet 3/2 {
    d8) e\downbow( fs a-0 b d
    e-1) fs_3\upbow( a_3_0 fs_3 e d-3 |
    b8) a-0\downbow( b d e-1 fs \clef "treble"
    a-0\thumb) b-1\upbow(  d-3 b a fs-2 | \break

% bar 39
    e8) d\thumb\downbow( e fs a-0\thumb b
    d-1) e( fs  e d-1 b-1 |
    a8-0\thumb) fs-2_II\downbow( d fs-1 a-2-0 d-0\thumb
    fs-1-0) d-- fs-- a-2-0 fs--\downbow( a---0) |
  }
  d2-3-0\upbow \clef "bass"
  <d,,, fs'>2\downbow\ff |
  d,1\downbow\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \fourteen
  \header {
    title = "14"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 14.pdf"
% End:
