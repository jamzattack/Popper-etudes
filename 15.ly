\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

fifteen = \relative c'' {
  \tempo "Allegro Scherzando"
  \key g \major
  \time 6/8
  \clef "treble"
  \autoPageBreaksOff

% bar 1
  d8-2_I b-.\thumb( fs-.-1_II) g-2->( e'-3_I) d-. |
  b8-2 g-.\thumb( ds-1-.) e-2->( c'-3) b-. |
  g8-2 e-.\thumb( b-1) c-2->( a'-3) g-. | \clef "tenor"
  e8-2 c-.\thumb( gs-1-.) a-2->( f'-3) e-. | \clef "bass"
  a,8-0\<( c) g-.-4 fs( c'-2) e,-. | \break

% bar 6
  d8\<( d'-3-0) c,_"III" b( d'-3-0) a,-1 |
  g8->\!( b) d_"II" g bf-.-1_I( cs-.-1) | \clef "tenor"
  e8-4->( d) e-1 f g-.-1( a-.) |
  b8-.-2 g-.\thumb( ds-1-.) e-2->( c'-3) b |
  g8-2 e-.\thumb( b-.) c-2->( a'-3) g-2 |
  e8-2 c-.\thumb( gs-.) a-2->( f'-3) e-. | \clef "bass" \break

% bar 12
  c8-2 a-0-.( e-.-1) f-2( d'-4) c |
  d,8\f( f-2_II d'-2-0) fs,-1( e'-4_I d\upbow) |
  d,8\<_II( g-2 d'_0_2) b_1_I( a'_2_0 g_4\upbow) |
  d8-1( e-1 es-2) f-3( \clef "tenor" e'-0-3 d-.-.\upbow) | \clef "treble"
  g,8-2_II( b\thumb_"I" e) d-2( b'_2-0 g_3\upbow) | \break

% bar 17
  e8\f-1( g-3 e'-3-0) a,,\thumb( c-2 a'-2-0) |
  d,,8\thumb_"II"( fs-2 d'-2-0_0) \clef "bass" d,,,4.\ff\downbow\fermata |
  ef8\p-2\downbow g-.( c-.) bf->( f'-4) ef |
  g,8\thumb bf-.-2( f'-.-4) ef-2( c'-4) bf-2 |
  bf8->( af) bf c f,-.-1( g-.) |
  af8 f-.-1( d-.) bf-2 af-.( f-.) | \break

% bar 23
  ef8 g-.( c-.) bf( f'-4) ef |
  g,8 bf-.( f'-._4) ef( c'-4) bf |
  bf8->( a!) c-.-1_II ef d-.( c-.) |
  bf8-2 a g-4 fs ef d |
  cs8-1_III e!-4( bf'-3_II\<) g-1->( f'_"I") e! | \break

% bar 28
  ds,8-1 fs-.-4( c'-.-3_I) a-0->( g'-4) fs-. |
  es,8-1 gs-.-4( d'-.-3) b!-1\downbow( a'_2) gs_1\upbow( |
  a8) gs-.\downbow( a-. gs-. a-. gs-. |
  a8-.) gs-.\upbow( a-. gs-. a-.) gs\downbow( |
  b8-3\f) a-.( g!-._1 fs-._3 e-. d-._4) |
  cs8-.\> b-. a-. g-. fs-. e-. | \break

% bar 34
  d8\p-. fs-. a-.-0 c!-.-1 a-. e'-.-4 |
  c8-. a-. g'-.-4 fs4._>\upbow |
  d,8-.-0 fs-. a-.-0 c!-.-1 a-. e'-.-4 |
  c8-. a-. g'-.-4 fs4._>\upbow |
  ds,8-.-1 fs-.-4 a-. c-.-3 a-. e'-.-2 |
  ds8-. a-.-0 g'-.-4 fs4.-> | \break

% bar 40
  ds,8-.-1 fs-.-4 a-. c-. a-. e'-.-1 |
  ds8-. a-. g'-.-4 fs4.-> |
  e,8-. g-. b-.\< d!-. c-. e-.-1 |
  g8-. e-.\! b-.-3 c-.\> a-.-1 g-.-4 |
  e8-.\! g-. b-. d!-.\< c-. e-.-1 |
  g8-. e-. b-.-3\! c-.\> a-.-1 g-.-4 |

% bar 46
  e8-.\< g-. b-. d-. c-. e-.-1 |
  g8 fs-3 \clef "tenor" e'-0_3 ef4.\f_2\fermata | \clef "treble"
  \grace { ef16-2( f } e8-.) c-.\thumb af-.-2 g-.-1 af-. c-. |
  ef8-.\prall^\markup{\tiny \natural}\cresc c-.\thumb af-. g-. af-. c-. |
  ef8-.\prall^\markup{\tiny \natural} c-. af-. g-. af-. c-. |
  ef8-.\f\prall^\markup{\tiny \natural} c-.\thumb ef_2\downbow(
  c'-0_2) bf-.-2\upbow( af-.-1) | \break

% bar 52
  f8-1_>\ff\downbow( af-3) df,-1_>\upbow( f-3) bf,-1_>\downbow( df-3) |
  af8-1_>\upbow( bf-2) f-1_>( af-4) df,-1_>( f-4) | \clef "bass"
  bf,8-1\ff( df-4) a!-.-0_> bf( df) ff,-.-2_> |
  bf8( df) ef,-.-1_> bf'( df) ff,-.-2_> |
  bf8( df) a!-.-0_> bf( df) f,-.-3_> | \break

% bar 57
  bf8( df) e,!-.-2_> bf'( df) f,-._> |
  bf8( df) a!-._> bf( df) gf,-.-4_> |
  bf8( df) f,-.-3_> bf( df) gf,-.-4_> |
  cs8-1\cresc( e!) bs-.-1_> cs-1( e) g,-.-2 |
  cs8( e) fs,-.-1_> cs'( e) g,-.-2 | \pageBreak

% PAGE TWO

% bar 62
  e'8-1( g) ds-.-1 e( g) bf,-.-2_> |
  e8( g) a,-.-1 e'( g) bf,-.-2_> |
  b!8\ff-.-3 d-.-0-3 g-.-4 fs-. e-. d-.-4 |
  b8-. a-.-0 g-. e-.\> d-.-0 b-. |
  g8-.-0 bf-.-1 cs-.\! e-.\< g-.-4 bf-.-1 |
  cs8-.-3 \clef "tenor" e-1_"rit." g(
  \tuplet 2/3 { e'-0-3\fermata) ef-2\upbow } | \break

% bar 68
  d8\prall-2\f b!-.\thumb( fs-.-1) g-2_>( e'!-3) d-. | \clef "treble"
  b8-2 g-.\thumb( ds-.-1) e-.^>( c'-3) b-. |
  g8-2 e-.\thumb( b-.) c-2->( a'-3) g-. | \clef "tenor"
  e8-2 c-.\thumb( gs-.) a-2->( f'-3) e-2 | \clef "bass"
  a,8-0\<( c) g-. fs( c') e,-. | \break

% bar 73
  d8( d'-0-3) c,-4_III b-3( d'-0-3) a,-1 |
  g8\!( b) d_"II" g bf-.-1_I( cs-.-1) | \clef "tenor"
  e8-4->( d) e-1 fs g-.-1( a-.) |
  b8-2 g-.\thumb( ds-.-1) e-2->( c'-3) b-. |
  g8-2 e-.\thumb( b-.) c_2->( a'_3) g-._2 |
  e8-2 c-.\thumb( gs-.) a-2->( f'-3) e-. | \clef "bass" \break

% bar 79
  c8-2 a-.-0( e-.-1) f->( d'-4) c-2 |
  d,\f-0_II( f-2 d'_0_2) fs,-1\(( e'-4_I) d-1_\upbow\<\) |
  d,8_"II"( g-2 d'-0-2) b-1\(( a'_0_2) g-._4\upbow\) |
  d8-1( e-1 es-2) fs_3( \clef "tenor" e'_0_3 d-._2\upbow) | \clef "treble"
  g,8-2_II( b\thumb_"I" e) d( b'_2-0 g-._3\upbow) | \break

% bar 84
  e8-1\f( g-3 e'-0_3) a,,\thumb( c-2 a'-0_2) |
  d,,8\thumb_"II"( fs-2 d'-0-2_0) \clef "bass" d,,,4.\ff\downbow\fermata |
  df8-.\p bf'-.-3 bff-. af-. df-.-4 f-.-1 |
  af8-.-4 f-. c-. df-. f-. af-. |
  df8-.-2 af-. e!-.-1 f-.-1 af-. df-.-2 | \break

% bar 89
  f8-.-4 df-.-1 g,!-.-2 af-.-2 df-.-1 f-.-4 | \clef "tenor"
  af8-.-2 f-.\thumb bf-. af-.\prall f-. df-.-2 |
  bf8-.\thumb af-.-3_III bf-._II df-.-2 ef-. f-.\thumb |
  af8-.-2 f-.\thumb bf-. af-.\prall f-. df-. |
  bf8-.\thumb af-.-3 bf-. df-. ef-. f-.\thumb | \break

% bar 94
  af8-. f-. bf-. af-.\prall f-. df-. |
  af'8-. f-. bf-. af-.\prall f-. df-. |
  gs8-. e!-.\thumb a!-.-3 gs-.\prall e-. cs-.-2 |
  gs'8-. e!-.\thumb a!-.-3 gs-.\prall e-. cs-.-2 |
  g'!8-.-2 e-.\thumb a-.-3 g-.\prall e-. cs-.-3 | \break

% bar 99
  g'!8-.-2 e-.\thumb a-.-3 g-.\prall e-. cs-.-3 |
  g'8-2 e\thumb a g e a |
  \new Voice
  <<
    { % top
      s2. | % empty bar for crescendo
      fs8-1 e\thumb d-3 e\thumb fs-1 a-0\thumb |
      d8-3 a\thumb d-3 a4.\thumb |
      fs8-1^I e\thumb fs e4.-> | \break

% bar 105 (top)
      fs8-1 e\thumb fs e4.-> |
      g8-1 fs\thumb e-3 fs g-1 \clef "treble" b\thumb |
      e-3 b\thumb e b4.\thumb | \clef "tenor"
      g8-1 fs\thumb g fs4.-> |
      g8-1 fs\thumb g fs4.-> | \clef "treble"
      b8-1 a\thumb g a d a | \break

% bar 111 (top)
      b8 a\thumb g a b g |
      b8-> a g a d a |
      b8 a g a b g | \bar "||" \clef "bass"
    }
    { % bottom
      g8\< e a g e a | % put this bar here for the crescendo
      d,8\f a_\thumb fs_2 a_\thumb d_3 e_1 |
      fs8_2 e_1 fs_2 e4. |
      d8_3_II\p a_\thumb d a4. | \break

% bar 105 (bottom)
      d8_3 a_\thumb d a4. |
      e'8\f_3 b_\thumb g_2 b e \clef "treble" fs_1 |
      g8_2 fs_1 g fs4._1 | \clef "tenor"
      e8_3_II\p b_\thumb e b4. |
      e8_3 b_\thumb e b4. | \clef "treble"
      g'8\f_3 d_\thumb b d fs d | \break 

% bar 111 (bottom)
      g8 d_\thumb b d g b, |
      g'8\p_( d) b d fs-._( d-.) |
      g8_( d) b d g-._( b,-.) | \bar "||" \clef "bass"
    }
  >>

% double bar line

  <<
    % double stops
      \new Voice
      << \stemUp
% top/top
	 {
	   f'2.-2^(^~ |
	   f2.^~ |
	   f2. |
	   e2. |
	   c2.-3^~ | \break

% bar 119 (top/top)
	   c2.-3) |
	   b2.-2^~ |
	   b2. |
	   b2. |
	   b2. |
	   \tempo "Presto"
	   d,8 r r d r r |
	   d8 r r d r r | \break

% bar 126 (top/top)
	   d8 r r g r r |
	   d'4-0^II\thumb r8 g r r | \clef "tenor"
	   g8-3 d-0\thumb g-3 d g-3 d |
	   g8 d g d g a-0\thumb |
	   b2.-1~ |
	   b2.~ |
	   b2. \bar "|." \pageBreak
	 }

% top/bottom
	 {
	   \override Fingering.staff-padding = #'()
	   s2. |
	   s2. |
	   b,2._3 |
	   c2._4 |
	   e,2._2 | \break

% bar 119 (top/bottom)
	   ef2._1 |
	   d2._0^~ |
	   d2. |
	   d2. |
	   d2. |
	   \tempo "Presto"
	   s2. |
	   s2. |
% bar 126 (top/bottom)
	   s2. |
	   s2. | \clef "tenor"
	   b'8_2 a_1 b_2 a_1 b_2 a_1 |
	   b8 a b a b d |
	   g2._3~ |
	   g2. |
	   g2. \bar "|." \pageBreak
% end (top/bottom)
	 }
       >>
    

    \\

% bottom
    {
% double bar (bottom)
      s2. |
      s2. |
      r4 r8 g,,4_"pizz." r8 |
      r4 r8 g4 r8 |
      r4 r8 g4 r8 | \break

% bar 119 (bottom)
      r4 r8 g4 r8 |
      r4 r8\dim g4 r8 |
      r4 r8 g4 r8 |
      r4 r8 g4 r8 |
      r4 r8 g4 r8 |
      \tempo "Presto"
      b8\mf_"arco" a g b a g |
      b8 a g b a g |

% bar 126 (bottom)
      b8 a g b a g |
      b'8_2_iii a_1\> g_0_\thumb b a g | \clef "tenor"
      s8\! s\> s2 |
      s2. |
      s2.\p |
      g4_0_\thumb_"pizz." r8 r4 r8 |
      g,4^0_2 r8 r4 r8 \bar "|." \pageBreak
    }
  >>
}


\score {
  \new Staff \fifteen
  \header {
    title = "15"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 15.pdf"
% End:
