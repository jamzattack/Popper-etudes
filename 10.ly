\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

ten = \relative c, {
  \tempo "Appassionato"
  \key c \minor
  \time 4/4
  \clef "bass"

% bar 1
  c16\f_0( g'_0 ef'_4 d c d c g_0)
  f_2( g f ef_2 d_1 ef d c) |
  b'16-2( af'-4 fs g ef'8.-4-> d16)
  bf,16-1( af'-4 fs-2 g-3 df'8.-2-> c16) | \break

% bar 3
  a,16-1( g' e f d'8.-4-> c16)
  af,16-1( g'-4 es-2 fs d'8.-4-> c16~ |
  c16\<) b( af'_4 g_3 fs_2 f_1 d_4 bf_1
  af-4) g( fs f d-0 b_4 af g-0) | \break

% bar 5
  c,16\f_0( g'_0 ef'_4 d c d c g)
  f_2( g f ef_2 d ef d c) |
  bf'16-1\<( af'-4 fs g) f'!8.-2->( e16)
  a,,-1( g' e f) g'8._4->( f16) | \break

% bar 7
  af,,16-1( g'-4 es-2 fs-3) g'8._4->( fs16)
  g,,-0( af'-4 fs-2 g-3) a'8._4->( g16) |
  f,,16\f_1( af c_1 df f-1_III af c-1_II df
  f_"I"\thumb) bf-3( af-2 f\thumb ef_3_II df_2 af_3_III f_1 | \break

% bar 9
  gf16-2) bff'-3( gf ef-3_II c-1 bff-4 gf-1 ef-4
  c) gf''-3( ef-1 c-4 bff-1 gf-4 ef-1 c-4 |
  bff16) ef'-4_I( c bff-0 gf-4 ef-1 c-4 bff-1
  gf-4) c'-4( bff-1 gf-4 ef-1 c-4 bff-1 gf-2) | \break

% bar 11
  f16_1( af c_1_III df f-1 af c-1_II df
  f\thumb_"I") bf-3( af f ef-3_II df af-3_III f) |
  g,!16-0( bf d-1_III ef g-1 bf d-1_II ef \clef "tenor"
  g\thumb_"I") c-3( bf-2 g f-3_II ef-2 bf-3_III g-1) | \clef "bass" \break

% bar 13
  a,16( c e-1_II f a-1 c e-1_I f \clef "tenor"
  a_0_\thumb) df_3( c_2 af\thumb gf-3_II f-2 ef-1 c-3_III) |
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 { % weird triplet structure
    bf16-2( d\thumb-0 d'-3) d( d, bf)
    a-1( d cs'-3) cs( d, a)
    af-1( d c'-3) c( d, af) \clef "bass"
    g,_0( d''\thumb \clef "tenor" c')
  }
  c([ b] | \break

% bar 15
  c16) g-1( af!\prall-2 g-1 f\thumb g-1 f ef_"II"
  d-2) ef( d c-1 af-2 bf-4 af g-1) |
  fs16-2\downbow( a-0 c-1 ef-4 d af'-4 b,-2 g'-3) \clef "bass"
  e,-1\upbow( g-4 bf-1_I df-3 c-2 gf'-3 a,-1 f'-2) | \pageBreak

% PAGE TWO

% bar 17
  d,16-0\downbow( f a-0 bf-1 fs'-3 g e-1 f
  d-1 ef cs-1 d b-1 c a-0 bf-1) |
  g,16-0\<\upbow( d'-0 c'-2 b-1 af'-4) g,,-1\downbow( af-2 fs-1
  g e-1 f cs-1 d-2) a'_3\upbow( fs_1 g_2) | \break

% bar 19
  c,16\ff_0( g'_0 ef'_4 d c_1 d c g_0)
  f_2( g f ef_2 d_1 ef d c) |
  cs16( g'_0 e'_4 d cs_1 d cs g_0)
  f_2( g f e d_2 e d cs) | \break

% bar 21
  d16_1( a' f'-2 e d-0 e f a-0
  d-4) a( f e f a d-1 e |
  f16) e( d a-0 d-1 e f \clef "tenor" a\thumb-0 \clef "treble"
  d-3) a\thumb-0( f-2 d\thumb-0 f\thumb a-2 d-1 f-3) | \break

% bar 23
  f16-3( df-1 af-2 f\thumb d\thumb-0 g-3 b-1 d-3)
  d( bf-1 f-2 d\thumb bf_\thumb df_2 gf_1 bf_3) |
  bf16( g!_1 ef_3 bf_\thumb g!_\thumb bf_2 ef_1 g_3)
  g\<( ef bf g \clef "bass" d-0 f-2 b-1 d-4) | \break

% bar 25
  c16\!-2->( g ef d-0 c g ef d
  c_0\<) ef_1( fs_4 a_1 c-4 ef-1 fs-3 a-0 |
  c16->\!) g( ef d-0 c g ef d
  c) ef( fs a c ef fs a | \break

% bar 27
  c16-1) d( ef d c g_3 c d
  ef) f-2( g f ef c_4 ef f | \clef "treble"
  g16-4) b-2( c-3 af-1 g\thumb ef-2 g bf-2
  c-1) d-2( ef-3 d c g-2 c d | \break

% bar 29
  ef16-3) d( c d c af-1_I g\thumb af g8)
  <<
    \new Voice {
      \stemUp
      \slurUp
      \tieUp
      g8\upbow~ g4 |
      g4\downbow~ g16 s8. s8 \clef "bass"
      g,8\thumb\upbow~( g4~ | \break

% bar 31 (top)
      g16) r g8~( g4~ g16) r g8~( g4~ |
      g16) r g8~( g4 g16) r g8~ g4 | \break
    }
    \new Voice {
      \stemDown
      \slurDown
      c16_\thumb( d ef_2 g_4 d_1 f_3) |
      ef16( d c_0 d_1 c af-1_II g\thumb af
      g8) \clef "bass" c,16_\thumb( d ef f d-1 ef-2 | \break

% bar 31 (bottom)
      c16) g_1( c d ef f d ef
      c) g_1( c d ef f d ef |
      c16\p) g( c d ef f d ef
      c) g( c d ef f d ef) | \break
    }
  >>

% bar 33
  c,16\pp_0( g'_0 ef'_4 d c d c g
  f-2 g f ef-2 d-1 ef d c) |
  c16\ppp( g' ef'-4 d c-1 d c g_0
  c,16 g' ef' d c d c g) |
  c,1\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \ten
  \header {
    title = "10"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 10.pdf"
% End:
