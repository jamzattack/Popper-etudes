.PHONY: all
.PHONY: clean

LY = $(wildcard *.ly)
PDF = ${LY:.ly=.pdf}

etudes.pdf: ${LY}
	lilypond etudes.ly

all: ${PDF}

clean:
	rm *.pdf *.midi

%.pdf %.midi: %.ly
	lilypond $<
