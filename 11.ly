\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

eleven = \relative c, {
  \tempo "Moderato"
  \key f \major
  \time 4/4
  \clef "bass"

% hide triplet things, as this one is mostly triplets
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f

% bar 1
  \tuplet 3/2 {
    f8\mf a' e f d' c
    c, a' e f d' cs |
    d,8 bf' fs-3 g-3 ef'-4 d
    d,-0 bf'-4 g df-2 bf' g | \break

% bar 3
    c,8-1\< bf' fs-1 g-1 f'!-4 e
    e,-1\! d'\> df c, c' bf |
    a8-0\! c e, f a b,
    c f gs,_1 a_1 c e, | \break

% bar 5
    f8\p a e'  d a g'
    f d bf' \tuplet 2/3 { a4-> } |
    fs,8-4 a-1 g'-4 fs c bf'-1
    a-0 fs-2 ef'-4 \tuplet 2/3 { d4->-3 } |
    g,,8-0\< bf'-1 f!-3 e-2 df'-4 bf-1
    a,-2 c'-1 g-3 fs-2 ef'-4 c-1 | \break

% bar 8
    b,8-2\< d'-1 a-3 gs f'-4 d
    cs,-2 e'-1 b-3 as g' e |
    ds8-1\f\> fs a,!-2 gs-2 f' d,-3
    cs-3 e'-4 g,-2 fs-2 ef'-4 c,-3 | \break

% bar 10
    b8-3 d'-4 f,!-2 e-2 df'-4 bf,-3\!
    a-2\< cs,-1 e-2 a-1 cs-4 e |
    a8-0\f cs-3 a, bf-> c' bf,
    a-> c' a, g-> c' g, | \break

% bar 12
    a8 cs' a, bf cs' bf,
    a cs' a, g cs' g, |
    a8 cs' a, bf g' bf,
    a e' a, g e''-1 g,, |
    a8 cs' a, bf g''-4 bf,,
    a\> c' a, g e''-1 g,, | \break

% bar 15
    f8\p a' e f d' c!
    c, a' e f d' cs |
    d,8 bf' fs-3 g-3 ef'-4 d
    d,-0 bf'-4 g df-2 bf' g |
    c,8-1 bf' fs-1 g-1 f'!-4 e
    e,-1 d' df c,-4 c' bf | \break

% bar 18
    a8-0 c e, f a b,
    c f gs,-1 a-1 c e, |
    f8 a d c a g'
    f c a'-0 \tuplet 2/3 { c4-> } |
    f,,8\< a e' d a g'
    f d a' \tuplet 2/3 { d4-> } | \pageBreak
    
% PAGE TWO

% bar 21
    g,,8 bf ef d bf a'
    g d bf' \tuplet 2/3 { d4-> } |
    g,,8 bf f' ef bf a'
    g e bf'-1 \tuplet 2/3 { ef4-4-> } |
    ef8-4->\f( c-1) gs-4 a-0 bf-1 b-2
    c-3\cresc( a-0) g'-4 \tuplet 2/3 { fs4\upbow } | \break

% bar 24
    e!8-4->( cs) a-0 as-1 b-2 bs-3
    cs-4( as-1) af'-4 \tuplet 2/3 { g!4-3\upbow } |
    f!8-4->\ff( d) as-1 b-1 c! cs
    d->( b) g!-4 gs-1 a as | \break

% bar 26
    b8-4->( gs) e-1 f!-1 fs-2 fss-3
    gs->-4( es) cs-1 d-0 ds-1 e-2 |
    f!8-3( d-0) gs-4 f-1( d) c'-4
    b-4( af-1) g'-4 \tuplet 2/3 { f4-2\downbow } | \break

% bar 28
    fs,8-4\upbow( ds) a'-0 fs( ds) cs'-4
    c-3( a-0) g'-4\downbow \tuplet 2/3 { fs4\upbow } |
    g8->\ff\downbow( e) bs-1 cs-1 d-2 ds-3
    e-4->( cs) a!-0 as-1 b-2 bs-3 | \break

% bar 30
    cs8-4->( as) fs-4 g!-1 af-2 a-3
    bf-4( g) ds-1 e-1 f fs-3 |
    g8-4\p( e) df'-3 c( e,) g
    a-0( f) d'-4 c( f,) a-0 | \break

% bar 32
    a8-0( fs-1) ef'-3 d-2( fs,) a-0
    bf-4( g) ef'-2 d-1( g,) bf |
    bf-. g-. f'-.-4 e!-.-3 g,-. ef'-.-2 d-.-4\<
    e,-. df'-.-3 c-.-2 e,-. b'-. | \break

% bar 34
    bf8-.-4_II df,-.-2_III a'-.-3_II af-.-4 bf,-.-1 g'-.-3
    fs-.-3 g,-.-0 f'-.-2 e-. c-. c'-. |
    f,,8\f a'-0 e f-1 ef'-4 c
    f,-2 d' b f-3 df'-4 bf | \break

% bar 36
    f8-2 c' a-0 f b-1 gs-4
    f-2 bf-1 g-4 e bf g |
    f8 c''-4_II gs-1 a-2 gf'-4_I ef-1
    gs,-2_II f'-4_I d g,-2 e'-4 cs | \break

% bar 38
    gf8-2 ef'-4 c f,-2 d'-4 b
    e,-2\> df' c bf a g |
    f8\p\< c a' f d' c
    a-0\! g'-4\> f d-4 c a | \break

% bar 40
    f8\< c a' f d' c
    a\! g'\> f d c a |
    f8\p c a' f d' c
    f, c a' f d' c |
  }
  <a, f'>1 \bar "|." \pageBreak
% end
}

\score {
  \new Staff \eleven
  \header {
    title = "11"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 11.pdf"
% End:
