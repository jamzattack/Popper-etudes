\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

NUMBER = \relative c' {
  \tempo "Allegro"
  \key c \major
  \time 4/4
  \clef "bass"

% bar 1
}

\score {
  \new Staff \NUMBER
  \header {
    title = "NUMBER"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B NUMBER.pdf"
% End:
