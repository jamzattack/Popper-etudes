\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

one = \relative c {
  \tempo "Allegro molto moderato"
  \key c \major
  \time 4/4
  \clef "bass"
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 { % All notes but the last bar are triplets
% bar 1
    c8\mf\upbow c' b a c g
    f a e d f c |
    b8\upbow d' c b d a
    g b f e g d |
    c8-2\upbow e'-4 d c-1 e b-1
    a-0 c g f a e | \break

% bar 4
    d8-0\upbow f'-4 e d f c-2
    b d a g b f |
    e8\upbow g'-4 f e g d-2
    cs-1 e a, d-1 f c-2 |
    b8\upbow d g, c-1 e b-1
    a-0 c f, b d a | \break

% bar 7
    gs8-4\upbow b-1 e,-1 f a-0 d,
    c-4 e-1 a, gs-1 b-4 e,-4 |
    a8-1\upbow c'-2 b a-0 e'-4 a,
    gs-3 e' gs, g-2 e' g, |
    f8-1\upbow e'-4 d d,-0 c' b
    b,-1 a'-4 gs gs,-1 f'-3 e-2 | \break

% bar 10
    a,8-1\upbow c'-2 b a-0 e'-4 a,
    gs-3 e' gs, g-2 e' g, |
    f8-1\upbow e'-4 d d,-0 c' b
    b,-1 a'-4 gs gs,-1 f'-3 e-2 |
    a,8-1\upbow c'-2 b a-0 g'-4 e-1
    fs,-2_\markup {\small \italic "light"} ds'-4 c-1 a-2 fs'-4 ds-1 | \break

% bar 13
    \override Beam.auto-knee-gap = #3  % use kneed beams
    f,!8-2\upbow d'!-4 b gs-2 f'!-4 d
    e,-1 c'-2 a-0 a, c' a\downbow |
    f, c''-> a e,-> c'' a
    d,,-> c'' a  ds,,-> c'' a |
    e,-> c'' a f'-2 e a,-0
    e,->-3 b''-4 gs-1 f'-3 e-2 gs, | \break

% bar 16
    a8-0\upbow c-2 b a-0 g'-4 e-1
    fs,-2 ds'-4 c-1 a-2 fs'-4 ds-1 |
    f,!8-2 d'!-4 b gs-2 f'!-4 d
    e,-1 c'-2 a-0 a, c' a\downbow |
    f,8 c''-> a e,-> c'' a
    d,,-> c'' a  ds,,-2-> c'' a | \break

% bar 19
    e,8->\upbow c'' a f'-2 e a,-0
    e,->-3 b''-4 gs-1 f'-3 e-2 gs, |
    a8-0 c-2 b a bf'-3 g-1
    fs-1 a-3 c,-2 b!-2 af'-4 f-1 |
    e8-1 g-4 bf,-2 a-2 fs'-4 ds-1
    d-1 f-4 af,-2 g e'-4 cs-1 | \break
    
% bar 22
    c!8-1 ef-4 d c_\markup {\italic "leaping"} \clef "tenor" c'-3 a
    gs-1 b d,-2 cs-2 bf'-3 g-1 |
    fs8-1 a-3 c,-2 b!-2 af'-4 f-1
    e-1 g-4 bf,-2 a-2 fs'-4 ds-1 |
    d!8-1 f!-4 e d_\markup {\italic "leaping"} d'-3 b-1
    bf-1 cs-3 e,-2 ds-2 c'-3 a-1 | \break

% bar 25
    gs8-1 b-3 d,-2 cs-2 bf'-3 g-1
    fs-1 a-3 c,-2 b-2 af'-4 f-1 |
    e8-1 g bf,-2 e g-1 bf
    g bf cs,-2 g'-1 bf-1 cs-3 |
    bf8-1 d-3 d,\thumb g-3 bf-1 d
    a\thumb d-3 d, fs-2 a d-3 | \break

% bar 28
    g,,8-G\thumb\upbow bf-2\p d,-1 g bf d\thumb
    bf d g, bf d g-3 |
    g8-- a\thumb d, g a d-3
    fs, a d, fs a d-3 |
    f,,8_G\thumb\upbow af c,-1 f af c\thumb
    af-2\upbow c f, af c f!-3 | \pageBreak

% PAGE TWO

% bar 31
    f8---3 g\thumb c,\thumb f g c
    e,!---2 g c, e g c-3 |
    ef,8-2_D c'^A d,-1 c-1 a'-2 bf,-2
    a-1 fs'-2 g,-2 fs ef'-3 d |
    df8-2 bf'-3 c,-1 bf-2 g'!-4 af,-1
    g-2 e'!-4 f,!-2 e! df'!-3 c | \break

% bar 34
    \clef "bass"
    cf8-2_D af'-4 bf,-1 af-2 f'-4 gf,-1
    f-2 d'!-4 ef, d!-0 cf'!-3 bf!-2 |
    df,8-1 cf'-4 bf c,-1 bf'-4 a!
    b,-1 a'-4 af bf,-1 af'-4 g! |
    a,!8-1 g'-4 fs af,-1 fs'-4 f
    g,-0 f' d f d f | \break

% bar 37
    g,8\p\upbow-0 af'-4 f af f af
    g, af' f b---4 af b |
    g,8-0 af'-4 f af f af
    g, af' f b---4 af b |
    g,8\> af' f af f af
    g, af' f af f af |
    g,8 g' f g f g
    g, g' f g f g | \break
    
% bar 41
    c,8-4\p\upbow c' b a c g
    f\upbow a e d f c |
    b8 d' c b d a
    g b f e g d |
    c8-2 e'-4 d c-1 e b-1
    a-0 c g f a e | \break

% bar 44
    d8\upbow f'-4 e d f c-2
    b d a g b f |
    e8\upbow g'-4 f e g d-2
    cs-1 e a, d-1 f c-2 |
    b8 d g, c-1 e b-1
    a-0 c f, b d a | \break

% bar 47
    gs8-4 b-1 e,-1 f a-0 d,
    c-4 e-1 a, gs-1 b-4 e,-4 |
    a8-1 c'-2 b a-0 e'-4 a,
    gs-3 e' gs, g-2 e' g, |
    f8-1 e'-4 d d,-0 c' b
    b,-1 a'-4 gs gs,-1 f'-3 e-2 | \break

% bar 50
    a,8-1\upbow c'-2 b a-0 e'-4 a,
    gs-3 e' gs, g-2 e' g, |
    f8-1 e'-4 d d,-0 c' b
    b,-1 a'-4 gs gs,-1 f'-3 e-2 |
    a,8-1 c'-2 b a-0 g'-4 e-1
    fs,-2 ds'-4 c-1 a-2 fs'-4 ds-1 | \break

% bar 53
    f,!8-2\upbow d'!-4 b gs-2 f'!-4 d
    e,-1 c'-2 a-0 a, c' a\downbow |
    f, c''-> a e,-> c'' a
    d,,-> c'' a  ds,,-> c'' a |
    e,-> c'' a f'-2 e a,-0
    e,->-3 b''-4 gs-1 f'-3 e-2 gs, | \break

% bar 56
    a8-0\upbow c-2 b a-0 g'-4 e-1
    fs,-2 ds'-4 c-1 a-2 fs'-4 ds-1 |
    f,!8-2 d'!-4 b gs-2 f'!-4 d
    e,-1 c'-2 a-0 a,\< c' a\downbow |
    f,8 c''-> a e,-> c'' a
    f,-> c''-4 a fs,-4-> c'' a | \break

% bar 59
    g,-0\f e''-1 c g'-4 e c
    g, f''-2 b,-3 g'-4\> f b, |
    c,,-0\!\upbow e''\mf-1 bf g'-4 e bf
    c,,-0 ef''-1 a,-2 fs'-4 ef a, |
    c,,-0\upbow d''-1 af-2 f' d af
    c,,-0\upbow c''-1 g-2 e' c g | \break

% bar 62
    c,,-0\upbow a''-0\dim fs-3 d' a fs
    c,-0\upbow af''-4 f d'-3 af f |
    c,-0\upbow\> g''-4 e c' g e
    g,\upbow e' c g' e c |
    e,\upbow c' g e' c g
    c, g' e c' g( e) |
  } %end of triplets
  <c c'>1\p\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \one
  \header {
    title = "1"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 01.pdf"
% End:
