\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

twelve = \relative c'' {
  \tempo "Allegro"
  \key c \major
  \time 4/4
  \clef "treble"
  \autoPageBreaksOff % Lilypond was trying to put the last line on its own page

% bar 1
  c16\f-3( g\thumb e-2 c\thumb ds-1-> fs-3 a-1 c-3)
  c( g\thumb e c d-> f a c) |
  c16( g\thumb e c\thumb e\thumb g-2 c-1 e-3)
  e( c g e g\thumb c-3 e-1 g-3) | \break

% bar 3
  g16-3( d\thumb b-2 g\thumb as-1-> cs-3 e-1 g-3)
  g( d b g as-> cs e g) |
  g16( d b g d\thumb g-3 b-1 d-3)
  d( b g d b\thumb d-2 g-1 b-3) | \break

% bar 5
  b16( g-1 f\thumb d-2-II f-2 g-3 b-1_I d-3)
  d( c g e-1_II g-1 c\thumb_"I" f-3 e) |
  e16-2( cs-2 a\thumb g-3 e-1 g a cs-2
  e-2) d-1( a-2 f\thumb a-2 d-1 e-2 f-3) | \break

% bar 7
  f16-3( d-1 b'-3 a-2 f-2 d\thumb b-2_II a-1
  f-2) d-0\thumb( b'-1_I a-0\thumb \clef "bass" f-2_II d-0\thumb b-3 a-1 |
  f16-2) d-0( b'-3 a-1 f-2 d-0 b-3_III a
  f-4_IV) d-1( b'-3 a g-0 a'-4_II fs-1 g-2) | \break
% allow fermata to be at end of line
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup{ \fermata }

% bar 9
  c16-4->_II\f( e-1_I g-4 e b-3->_II e_"I" g e)
  a,-0->( e' g e b-3->_II e_"I" g e) |
  c16-4->( e g e b-3-> e g e)
  a,-0->( e'-1 g e b-3-> e g e) | \break

% bar 11
  c16-4( e g-4 e \clef "tenor" a-1 g\thumb c-3 a-1 \clef "treble"
  e'-3-0) b-2( c-3 g\thumb a e g c,\thumb) | \clef "bass"
  c16\thumb( e-2 g\thumb e-2 \clef "tenor" a-1 g\thumb c-3 a-1 \clef "treble"
  e'-3-0) b-2( c-3 g\thumb a e g c,\thumb) | \break

% bar 13
  d16_>-2_II( f\thumb_"I" a-2 f cs_>-1_II f_"I" a f)
  c_>-1( f a f cs_>-1 f a f) |
  d16_>-2( f-0 a-2 f cs_>-1 f a f)
  c_>-1( f a f cs_>-1 f a f) | \pageBreak

% PAGE TWO

% bar 15
  d16-2_II( f\thumb_"I" a-2 f\thumb bf-1 a\thumb d-3 a
  f'-4) cs-2( d-3 a\thumb b-1 f-2 a\thumb d,\thumb) |
  d16\thumb-0_II( f-2 a\thumb-0 f-2 bf-1 a\thumb d-3 a
  f'-4) cs-2( d-3 a\thumb b-1 f-2 a\thumb d,\thumb) | \break

% bar 17
  d16_>-2_II( f\thumb_"I" b!-3 f cs_>-1_II f\thumb b-3 f)
  d_>-2( f\thumb b f e_>-3 f\thumb b f) |
  d16_>-2( f\thumb b! f cs_>-1 f\thumb b f)
  d_>-2( f\thumb b f e_>-3 f\thumb b-3 f) | \break

% bar 19
  d16( f\thumb_"I" b!-3 f d'-3 b-1 f'!-3 d-1
  b-1 d-3 b g-3_II f-2 g-3 f d\thumb-0) |
  d16\thumb( f-2 b-1 f d'-3 b f'-3 d
  b-1 d b g-3 f-2 g f d\thumb) | \break

% bar 21
  e16_>-2_II( g\thumb_"I" c-3 g ds_>-1 g\thumb c g)
  e_>-2( g\thumb c g f_>-3 a-1 c-3 a) |
  e16_>-2( g\thumb c g ds_>-1 g\thumb c g)
  e_>-2( g\thumb c g f_>-2 a-1 c-3 a) | \break

% bar 23
  e16-2( g\thumb c-3 g\thumb e'-2 c\thumb g'-4 e-2
  c\thumb) f-3( df-1c af\thumb c-2 af f_"II" |
  d!16-2) f\thumb_"I"( bf-3 f d'-2 bf\thumb f'-4 d
  bf\thumb) ef-3( c-1 bf\thumb g\thumb bf-2 g\thumb ef-2_II | \break

% bar 25
  c16-2) ef\thumb_"I"( af-3 ef c'-2 af\thumb ef'-4 c-2
  af\thumb) df-3( bf-1 af\thumb f\thumb af-2 f df-3_II |
  b!16-2) d!\thumb_"I"( g-3 d b'!-2 g\thumb d'!-4 b
  g\thumb) c-3( af-1 g\thumb ef\thumb g-2 ef c-2_II | \break

% bar 27
  af16-2) c\thumb^I( ef-2 c af'-3 ef\thumb c'-2 af\thumb
  g\thumb df'-3 bf-2 g ff-3_II df-1 bf-3_III g-1) |
  af16-2( c\thumb_"II" ef c af'-1_I ef-2_II c'-3_I af-1
  g\thumb) df'-3( bf-2 g\thumb ff-3_II df-1 bf-3_III g-1) | \break

% bar 29
  af16-2( c\thumb_"II" ef-2 c af'-1_I ef-2_II c'-3_I af-1
  ef'-3) c-1( af'-3 ef-1 c'-2 af-1 ef'-2 c-1) |
  af'2.-3\fermata r4 | \pageBreak

% PAGE THREE

% bar 31
  af,,16-3_I\downbow\mf( g-2 fs-1 ef' d-2 b!\thumb fs!-1_II g-2
  gf-3) f( e! df'-3_I c-2 a\thumb-0 e-1_II f |
  ff16-3) ef-2\dim( d-1 cf'-3_I bf-2 g\thumb d!-1_II ef-2
  d-3) cs-2( c-1 a'-3_I af-2 f\thumb c-1_II df-2 | \break

% bar 33
  c16-3\!) b!-2\<( bf-1 g'-3_I fs-2 ef-1 \clef "bass" c-2 a-0
  fs-3) ef( c-4 a-1 fs-4 ef-1 c-0 a'-1) |
  g16_0( c e g b-1 c a'_2_0 g_4)
  g,,_0( cs_4 e a_0 cs_1 e bf'_3 a) | \break

% bar 35
  g,,16-0( d' f-2 a-0 d-1 f bf a)
  g,,-0( d'-0 f-2 b!-1 d-1 f \clef "treble" c'-3 b-2) |
  c16-3\f( g\thumb e-2 c\thumb ds->-1 fs-3 a-1 c-3)
  c( g\thumb e c ds-> fs a c) | \break

% bar 37
  c16( g e c e\thumb g-2 c-1 e-3)
  e( c g e\thumb g\thumb c-3 e-1 g-3) |
  g16( d\thumb b-2 g\thumb as-1-> cs-3 e-1 g-3)
  g( d b g a-> c e g) | \break

% bar 39
  g16( d b g d\thumb-0 g-3 b-1 d-3)
  d( b g d b\thumb d-2 g-1 b-3) |
  b16( g f\thumb d-2_II f-2 g-3 b-1_I d-3)
  d( c g e-1_II g-1 c\thumb_"I" f-3 e) | \break

% bar 41
  e16-2( cs-2 a\thumb-0 g-3 e-1 g a c-2
  e-2-0) d-1( a-2 f\thumb a-2 d-1 e-2 f-3) |
  f16-3( d-1 b'-3 a-2 f-2 d\thumb b-2_II a-1
  f-2) d\thumb( b'-1_I a-0\thumb \clef "bass" f-2_II d\thumb b-3 a-1 | \break

% bar 43
  f16-2) d-0( b'-3 a-1 f-2 d b-3_III a
  f-4) d-1( b'-3 a g a'-4 fs-1 g-2) | \mark \markup { \fermata }
  c16->-4_II( e-1_I g e b->-3_II e_"I" g e)
  a,->-0( e'-1 g e b->-3_II e_"I" g e) | \break

% bar 45
  c16->( e g e b-> e g e)
  a,->-0( e'-1 g e b-> e g e) |
  c16-4( e g-4 e \clef "tenor" a-1 g\thumb c-3 a-1 \clef "treble"
  e'-3-0) b-2( c-3 g\thumb a e g c,\thumb) | \clef "bass" \break

% bar 47
  c16\thumb( e-2 g\thumb e-2 \clef "tenor" a-1 g\thumb c-3 a-1 \clef "treble"
  e'-0-3) b-2( c-3 g\thumb a-1 e-2 g\thumb c,\thumb) |
  d16_>-2_II( f\thumb_"I" a-2 f cs_>-1 f a f)
  c-1_>( f a f cs-1_> f a f) | \break

% bar 49
  d16-2_>_II( f_"I" a f cs_>-1 f a f)
  c_>-1( f a f cs_>-1 f a f) |
  d16_2( f_\thumb a_2 f_\thumb bf-1 a\thumb-0 d a
  f'-4) cs-2( d-3 a\thumb-0 b-1 f-2 a\thumb d,\thumb) | \break

% bar 51
  d16_0_\thumb( f_2 a_\thumb f bf-1 a d-3 a
  f'-4) cs-2( d-3 a\thumb-0 b_1 f_2 a_\thumb d,_\thumb) |
  d16_>-2_II( f_1\thumb b!-3 f cs_>-1 f\thumb b-3 f)
  d_>-2( f b f e_>-3 f b f) | \pageBreak

% PAGE FOUR

% bar 53
  d16_>_II( f_"I" b! f cs_> f b f)
  d_>( f b f e_> f b f) |
  d16( f\thumb b!-3 f d'-3 b-1 f'-3 d-1
  b-1 d-3 b g-3 f-2_II g-2 f d\thumb-0) | \break

% bar 55
  d16\thumb-0( f-2 b!-1 f d'-3 b-1 f'-3 d-1
  b-1 d-3 b g-3 f-2_II g-3 f d\thumb-0) |
  e16_>-2_II( g\thumb_"I" c-3 g ds_>-1 g\thumb c g)
  e_>-2( g c g f_>-3 a-1 c-3 a) | \break

% bar 57
  e16_>_II( g_"I" c g ds_> g c g)
  e_>( g c g f_> a c a ) |
  e16_2( g_\thumb c_3 g e'-3 c\thumb g'-4 e-2
  c\thumb) f-3( df-1 c\thumb af\thumb c-2 af f-2_II | \break

% bar 59
  d!16-2) f\thumb_"I"( bf-3 f d'-2 bf\thumb f'-4 d
  bf\thumb) ef-3( c-1 bf\thumb g\thumb bf-2 g\thumb ef-2_II |
  c16-2) ef\thumb_"I"( af-3 ef c'-2 af\thumb ef'-4 c-2
  af\thumb) df-3( bf-1 af f\thumb af-2 f df-2_II | \break

% bar 61
  b!16-2) d!\thumb-0_I( g-3 d b'!-2 g\thumb d'!-4 b
  g) c-3( af-1 g\thumb ef\thumb g-2 ef c-2_II |
  af16-2) c\thumb_"I"( ef-2 c f-1 ef\thumb af ef\thumb
  a,-1_II) gf'-3_I( f-2 df'-3 c a\thumb-0 f-2_II a,-1) | \break

% bar 63
  bf16-2( df\thumb_"I" f-2 df gf-1 f\thumb bf-3 f
  b,-1_II) af'-3_I( g-2 ef'-3 d-2 b\thumb g-2_II b,-1) |
  c16-2( ef\thumb_"I" g-2 ef af-1 g\thumb c-3 g\thumb
  cs,-1_II) bf'-3_I( a-2 f'-3 e cs\thumb a-2_II g-1) | \break

% bar 65
  f!16\thumb( e'-2 d-1 a'-4) e,\thumb( d'-2 c!-1 g'-4)
  d,\thumb( c'-2 b!-1 f'-4) c,\thumb( b'-2 a-1 e'-4) |
  b,16\thumb( a'-2 g-1 d'-3 c-2 a\thumb-0 e-1_II fs-2
  g-3) fs-2( f-2 d\thumb-0 \clef "bass" b-2_III g-0\thumb f-3_IV d-1) | \break

% bar 67
  c16\f-0_IV\thumb( e-2 g-0_III\thumb b-2 \clef "tenor" c-3 e-1_II g-3 c-2_I
  df-3) bf-1( af\thumb f-2_II df\thumb bf-2_III af-1 f-3_IV) |
  d!16-1\<( fs-3 a!-1_III cs-3 d!\thumb-0_II fs a\thumb_"I" d-3
  ef-3) c-1( bf\thumb g-2_II ef\thumb c-2_III bf-1 g-3_IV) | \break

% bar 69
  e16-1( gs-3 b-1_III ds-3 e!\thumb_"II" gs-2 \clef "treble" b!\thumb_"I" e!-3
  f!-3) d-1( c\thumb a-2_II f\thumb d-2_III c-1 a-1) |
  g16\thumb( g'-3_II fs-2 g a-0\thumb g-3 fs g
  gs-3) gs( a\thumb) a( bf-1) bf( b-2) b( | \break

% bar 71
  c16_3\ff)( g_\thumb e_2 c ds_>_1 fs_3 a_1 c_3)
  c( g e c ds_> fs a c) |
  c16( g e c e_\thumb g_2 c_1 e_3)
  e( c g e g\thumb c-3 e-1 g-3) | \break

% bar 73
  g16( e c g c\thumb e-2 g\thumb c-3)
  c\upbow( g e c) c\downbow( e g c) |
  c16\ff\upbow
  <<
    {
      c-3\downbow( b-3 a-3 g-3 f-3 e-3 d \clef "tenor"
      c-3) c\upbow( b-3 a-3 g-3 f-3 e-3 d-3) |
    }
    {
      c'_\thumb b_\thumb a_\thumb g_\thumb f_\thumb e_\thumb d_\thumb \clef "tenor"
      c_\thumb c b_\thumb a_\thumb g_\thumb f_\thumb e_\thumb d_\thumb |
    }
  >>
  \clef "bass" <c,_0 c'-4>1\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \twelve
  \header {
    title = "12"
  }
  \layout { }
  \midi {}
}

% Local Variables:
% compile-command: "make -B 12.pdf"
% End:
