\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

%% Commentary:
% Similarly to nine.ly, most of this etude is double stops.  Where
% there is only one part, it is written as though it were in the top
% part, with spacers in the bottom.

global = {
  \tempo "Allegro molto moderato"
  \key ef \major
  \time 6/8
  \clef "tenor"
  \autoPageBreaksOff
}

top = \relative c' {
  \global

% bar 1
  ef8-3^I^( f-3 g-3) g^( af-3 bf-3) |
  bf8-3^( c-3 d-3) ef4.-3\upbow |
  ef8 bf\thumb^( c-1 bf) g\thumb^( af-1 |
  g8\thumb) ef\thumb^( f-1 ef4.\thumb) | \break

% bar 5
  f8-3^( g-3 af-3) af^( bf c) | \clef "treble"
  c8^( d! e) f4.\upbow |
  f8 c\thumb^( d-1 c\thumb) af\thumb^( bf-1 |
  af8\thumb) f\thumb^( g-1 f4.) | \break

% bar 9
  d8\thumb^I g-3^( a-3 b-3) c-3^( d-3 |
  ef8-3) c-1^( ef-3 g4.-3) |
  ef,8\thumb^I af!-3^( bf-3 c) d^( ef |
  f8-3) df-1^( f af4.-3) | \break

% bar 13
  e,8\thumb^I a-3( b-3 cs-3) d-3( e-3 |
  fs8-3) d-1( fs-3 a4.-3-0) |
  bf,!16\thumb^( bf'!-3) a,\thumb^( a'-3) af,\thumb^( af'-3)
  g,\thumb^( g'-3) f,\thumb^( f'-3) ef,\thumb^( ef'-3) | \break

% bar 16
  d,16( d') c,( c') bf,( bf')
  af,( af') g,( g') f,\thumb( f'-3) | \clef "tenor"
  ef8-3^( f-3 g) g^( af bf) |
  bf8^( c d) ef4.\upbow | \break

% bar 19
  ef8 bf16\thumb bf c8-1^( bf) g16\thumb g af8->^( |
  g8) ef16\thumb ef f8-1->^( ef4.) |
  f8-3^( g-3 af-3) af( bf c) | \clef "treble"
  c8( d! e) f4.\upbow | \break

% bar 23
  f8 c16\thumb c df8-1->^( c8) af16\thumb af bf8-1->^( |
  af8) f16\thumb f g8-1->^( f4.\thumb) |
  f8\thumb bf,16^( bf') c,^( c')
  d,!^( d'!) ef,^( ef') f,^( f') | \break

% bar 26
  g,16\thumb^( g'-3) bf,-2^( ef-1) g,\thumb^( g') bf4.-3 |
  fs16-1( a-3 gs-3 f-1 e-1 g-3
  fs-3 ds-1 d-1 f-3 e-3 cs-1 | \break

% bar 28
  c!16-1) ef-3 d!-3 b-1 bf-1 df-3
  c!-3 a!-1 gs-1 b-3 bf-3 g-1 |
  \new Voice {
    \stemUp
    fs4.-1^( a-1) |
    c4.-1^~^( c4 b8-1) | \pageBreak

% PAGE TWO

% bar 31
    b2.-1 |
    b2.\upbow |
    b4.\thumb^( cs4-1 b8) |
    e4.-3^( b\thumb) |
    gs4.\thumb^( a4-1 gs8) |
    cs4.-3^( gs\thumb) | \break

% bar 37
    a4.-1^( b\thumb) |
    cs2.-1 |
    fs4.-3^~^( fs4 cs8\thumb) |
    e2.\thumb-0 |
    f4.-3^~^( f4 c8\thumb) |
    ef2.\thumb | \bar "||" \key ef \major \break

% bar 43
    ef4.-1^( c-1) |
    af4.-1^( f-1) | \clef "tenor"
    f2.\thumb |
    f2. \thumb |
    f2. |
    bf-3\fermata | \clef "bass" \bar "||" \break
  } % end of tremolos

% bar 49
  ef,,8-3^III f-3 g-3 g-3 af-3 bf-3 |
  bf8-3( c-3 d-3) ef4.-3 | \clef "tenor"
  ef'8-3^I bf\thumb^( c-1 bf) g\thumb^( af-1 |
  g8\thumb) ef\thumb^( f-1 ef4.) | \clef "bass"
  f,8-3^III^( g-3 af-3) af-3^( bf-3 c-3) | \break

% bar 54
  c8-3^( d!-3 e-3) f4. | \clef "treble"
  f'8-3^I c\thumb( df-1 c) af\thumb( bf-1 |
  af8\thumb) f\thumb( g f4.\thumb) |
  f8 s4 s4. | \break

% bar 58
  s4. bf'4.-3 |
  fs16-1( a-3 gs-3 f-1 e!-1 g-3
  fs-3 ds-1 d-1 f-3 e-3 cs-1 |
  c!16) ef!-3( d!-3 b-1 bf-1 df-3
  c!-3 a-1 gs-1 b-3 bf-3 g-1) | \clef "tenor" \break

% bar 61
  \new Voice {
    \stemUp
    fs2.-1 |
    g2.-1 |
    f!2.\thumb |
    f4.\thumb^( bf-3) |
    ef,4.-3^II f\thumb^I
  }
  g8\thumb( af-1 bf\thumb c-1) bf\thumb( d-2) | \break

% bar 67
  \new Voice {
    \stemUp
    ef4.-3 bf-3 | \clef "bass"
    ef,4.^II
  }
  c8-1^( af-3^III f-1) | \clef "tenor"
  \new Voice {
    \stemUp
    ef'4.-3^II f\thumb^I |
  }
  g8\thumb( af-1 bf\thumb c-1) bf\thumb( d-2) |
  \new Voice {
    \stemUp
    ef8-3( d-2 ef-3) df-3( c-2 df-3) | \break

% bar 72
    cf8-3( bf-2 cf-3) bf-3( a-2 bf-3) |
  }
  b4.-3~^( b4 fs8-.\thumb) |
  \repeat tremolo 12 { g!32-1\upbow( fs\thumb) } |
  \repeat tremolo 6 { b32-3\downbow( fs\thumb) }
  \repeat tremolo 6 { b32-3\upbow( fs\thumb) } |
  \new Voice {
    \stemUp
    <d^\thumb b'-1>2.\downbow | \break

% bar 77
    <d^\thumb b'-1>8\upbow
  }
  r8 r r4 r8 | \tempo "Allegro vivo"
  r4 r8 \clef "bass" <ef, df'>4\downbow r8 |
  <ef c'>4\downbow r8 <f c'>4\downbow r8 |
  <g ef'>4\downbow r8 <f d'>4\downbow r8\! |
  \new Voice {
    \stemUp
    <g ef'>2.\downbow |
    <g_\upbow ef'>2. |
% end
  }
}

bottom = \relative c {
  \global

% bar 1
  ef8\f_"II"_\thumb f_\thumb g_\thumb g_\thumb af_\thumb bf_\thumb |
  bf8_\thumb c_\thumb d_\thumb ef4._\thumb |
  ef8_\thumb g_2 af_3 g ef_2 c_\thumb |
  ef8_2 c_2 af_\thumb c4._2 | \break

% bar 5
  f,8_\thumb g_\thumb af_\thumb af bf c | \clef "treble"
  c8 d! e f4. |
  f8 af_2 bf_3 af_2 f_2 df_\thumb |
  f8_2 df_2 bf_\thumb df4. | \break

% bar 9
  b8_2_II g_\thumb a_\thumb b_\thumb c_\thumb d_\thumb |
  ef8_\thumb g_2 ef_\thumb g4._\thumb |
  c,8_2_II af_\thumb bf_\thumb c df ef |
  f8_\thumb af_2 f_\thumb af4._\thumb | \break

% bar 13
  cs,8_2_II a_\thumb b_\thumb c_\thumb d_\thumb e_\thumb |
  fs8_\thumb a_2 fs_\thumb a4._\thumb_0 |
  s2.\p\< | \break

% bar 16
  s2.\< | \clef "tenor"
  ef,8\f_\thumb( f_\thumb g g af bf |
  bf8 c d ef4. | \break

% bar 19
  ef8 g16 g af8_3_( g8_2) ef16_2 ef c8_\thumb_( |
  ef8) c16 c af8_\thumb_( c4._2) |
  f,8_\thumb g_\thumb af_\thumb af bf c | \clef "treble"
  c8 d! e f4. | \break

% bar 23
  f8_\thumb af16_2 af bf8_3_( af) f16_2 f df8_\thumb_( |
  f8) df16_2 df bf8_\thumb_( df4._2 |
  d!8_2 s4\p\< s4. | \break

% bar 26
  s4.\< bf'4._\thumb |
  a16\mf_\thumb c_2 b_2 gs_\thumb g_\thumb bf_2
  a!_2 fs_\thumb f_\thumb af_2 g!_2 e_\thumb | \break

% bar 28
  ef!16\> gf_2_( f!_2 d_\thumb cs_\thumb e_2
  ef_2 c_\thumb b_\thumb d_2 df_2 bf_\thumb)\! |
  \new Voice {
    \stemDown
    \repeat tremolo 6 { a32_\thumb\p c_2 }
    \repeat tremolo 6 { c_\thumb ef_2 } |
    \repeat tremolo 6 { ds32_\thumb fs_2 }
    \repeat tremolo 6 { ds fs } | \pageBreak

% bar 31
    \repeat tremolo 12 { ds32_\thumb fs_2 } |
    s2. | \bar "||" \key e \major
    \acciaccatura e8_\thumb \repeat tremolo 12 { gs32_2 e } |
    \repeat tremolo 12 { gs32 e } |
    \repeat tremolo 12 { e32_2 cs_\thumb } |
    \repeat tremolo 12 { e32_2 cs_\thumb } | \break

% bar 37
    \repeat tremolo 6 { cs32\<_( e_2 }
    \repeat tremolo 6 { e_\thumb gs_2)} |
    \repeat tremolo 12 { e32_\thumb a_3 } |
    \repeat tremolo 12 { as32_2\mf fs_\thumb } |
    \acciaccatura a!8_\thumb_0 \repeat tremolo 12 { c32_2\p a_\thumb } |
    \repeat tremolo 12 { a32_2\mf\< f_\thumb } |
    \acciaccatura af8_\thumb
    \repeat tremolo 12 { cf32_2\p af_\thumb } | \bar "||" \key ef \major \break

% bar 43
    \repeat tremolo 6 { g32_\thumb bf_2 }
    \repeat tremolo 6 { ef,_\thumb g_2 } |
    \repeat tremolo 6 { c,32_\thumb ef_2 }
    \repeat tremolo 6 { af,_\thumb c_2 } | \clef "tenor"
    \repeat tremolo 12 { bf32_\thumb ef_3 } |
    \repeat tremolo 6 { bf32_\thumb d_2 }
    \repeat tremolo 6 { bf_\thumb c_1 } |
    \repeat tremolo 12 { bf32 d } |
    d2._2 | \clef "bass" \bar "||" \break
  } % end of tremolos

% bar 49
  ef,,8\f_\thumb_"IV"_( f_\thumb g_\thumb) g_\thumb( af_\thumb bf_\thumb) |
  bf8_\thumb c_\thumb d_\thumb ef4. | \clef "tenor"
  ef'8_"II"_\thumb g_2 af_3 g ef_2 c_\thumb |
  ef8_2 c_2 af_\thumb c4. | \clef "bass"
  f,,8_\thumb_"IV" g_\thumb af_\thumb af_\thumb bf_\thumb c_\thumb | \break

% bar 54
  c8_\thumb d!_\thumb e_\thumb f4. | \clef "treble"
  f'8_\thumb af_2 bf_3 af f_2 df_\thumb |
  f8_2 df_2 bf_\thumb df4._2 |
  d!8 bf16_\thumb( bf'-3) c,_\thumb( c')
  d,_\thumb( d'!) ef,_\thumb( ef') f,_\thumb( f') | \break

% bar 58
  g,16\thumb( g'-3) bf,-2( ef-1) g,( g'-3) bf,4._\thumb |
  a16\mf_\thumb c_2 b_2 gs_\thumb g_\thumb bf_2
  a!_2 fs_\thumb f_\thumb af_2 g!_2 e_\thumb |
  ef16_\thumb\> gf_2 f!_2 d_\thumb cs_\thumb e_2
  ef_2 c_\thumb b_\thumb d_2 df_2 bf_\thumb\! | \clef "tenor" \break

% bar 61
  \new Voice {
    \stemDown
    \repeat tremolo 12 { a32_\thumb\< c_2 } |
    \repeat tremolo 12 { bf32_\thumb ef_3 } |
    \repeat tremolo 12 { bf32_\thumb ef_3 } |
    \repeat tremolo 12 { bf32_\thumb ef_2 } |
    ef,8\f_"III"_\thumb( f_1 g_2) bf_"III"_\thumb( c_1 d_2) |
  }
  ef8_2 f_3 g_2 af_3 g_2 f_1 | \break

% bar 67
  \new Voice {
    \stemDown
    g8_2( f_1 ef_\thumb) d_2( c_1 bf_\thumb) | \clef "bass"
    g8_2_III( f_1 ef_\thumb)
  }
  af8_3 c,_2_IV af_\thumb | \clef "tenor"
  \new Voice {
    \stemDown
    ef'8_\thumb_"III"( f_1 g_2) bf_\thumb_"II"( c_1 d_2) |
  }
  ef8_2 f_3 g_2 af_3 g_2 f_1 |
  \new Voice {
    \stemDown
    ef4._\thumb df_\thumb | \break

% bar 72
    cf4._\thumb bf_\thumb |
  }
  b4. b4 b8_\thumb |
  \repeat tremolo 12 { ds32_3\< b!_\thumb } |
  \repeat tremolo 6 { ds32_2\f b!_\thumb }
  \repeat tremolo 6 { ds32_2 b_\thumb } |
  \new Voice {
    \stemDown
    g,!2._0\ff | \break

% bar 77
    g8_"pizz."\fff
  }
  r8 r r4 r8 | \tempo "Allegro vivo"
  r4 r8 \clef "bass"
  g4\f r8 |
  af4\< r8 a4 r8 |
  bf4 r8 bf4 r8\! |
  \new Voice {
    \stemDown
    <ef, bf'>4\ff s2 |
    s2. |
  }
  ef2.~\downbow | ef4 r8 \bar "|." \pageBreak
% end
}

\score {
  \new Voice << \top \bottom >>
  \header {
    title = "13"
  }
  \layout { }
  \midi { }
}

% Local Variables:
% compile-command: "make -B 13.pdf"
% End:


