\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

six = \relative c {
  \tempo "Allegro"
  \key f \major
  \time 4/4
  \clef "bass"

% bar 1
  f16 a d c b c a f
  e g d' c b c g e |
  d16-0 f c' b a-0 b f d
  e-1 g d' c b c bf g |
  f16 a d c b c a f
  a->-0 c-2 g'-4 f e f c-4 a-1 | \break

% bar 4
  gs-> b f'-3 e ds e b gs
  g-> bf f'-4 ef d ef bf g |
  fs16-1 a ef'-3 d cs d a fs
  f-1 af ef'-4 df c df af f |
  e16-1 g df' c b c g e
  d!-0 f c' b a-0 b f d | \break

% bar 7
  c16\< d e f g a bf! c
  d c bf g e c bf\! g |
  f16 a' d c b c a f
  e g d' c b c g e |
  d16 f c' b a b f d
  e g d' c b c bf g | \break

% bar 10
  f16 a d c b c a f
  a-0 c-2 g'-4-> f e f c-4 a |
  gs16-1 b f'-3-> e ds e b gs
  g-1 bf f'-4-> ef d ef bf g |
  fs16-1 a ef'-3-> d cs d a fs
  f-1 af ef'-> df c df af f | \break
  
% bar 13
  e16-1 g df'-> c b c g e
  d!-0 f c'-> b a-0 b f d |
  c16\< d e f g a bf c
  d-> c bf g e c bf\! g |
  f16\p\< g a bf c d e f g->
  a-0\! g\> f e d c bf | \break

% bar 16
  a16\< bf c d e f g a-0\!
  bf-> c\> bf a g f e d |
  c16\< d e f g a bf c\!
  d-> c\> bf a g f e d |
  e16\< f g a bf c d-1 e\!
  f-> e\> d c-2 bf a-0 g f\! | \break

% bar 19
  g16-4 a-0 bf c \clef "tenor" d\< e-1 f g-1
  a-2 bf a g f-2 g f e |
  d16-2 e d c \clef "bass" bf-1 c bf a
  g-2 a g f e-1 f e d |
  c16-2 d c bf a-1 bf a g-0
  fs\f-1 ef' d cs d ef c-1 e | \break

% bar 22
  bf16-2 c a bf g8-0 g'
  a,16-1 g' f e f g ef f |
  d16 ef c d bf8 bf'-3
  cs,16-1 bf' a gs a-2 bf g-1 a |
  f16-2 g e-1 f d8 d'-4
  e,16-1 d' c b c d bf c | \break

% bar 25
  a16-0 bf g-4 a f8 f'-3
  gs,16-1 f' e ds-1 e gs, a b |
  a16\mf f'-3 e ds e a,-0 f' e
  gs,-1 f' e ds e-2 gs, f'-3 e |
  a,16-0 f' e ds e a, f' e
  gs, f' e ds e gs, f' e | \break

% bar 28
  a,16-0 f'-3 e ds e a, f'-2 e
  b-3 g' f e f b, g' f |
  e16-1 g a,-1 c d-1 f g,-1 b
  c-1 e f,-1 a-0 b-1 d e,-1 g |
  a16-0 c d,-0 f a c d,-0 f
  g-4 b-1 d, f g b d, f | \break
% Allow fermata to be at the end of a line
  \once \override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
  \mark \markup{ \fermata }

% bar 31
  af,16\p-1 f'-3 ef d-0 ef af, f' ef
  g, ff'-2 ef d-0 ef g, ff'-2 ef-1 |
  af,16-1 f'!-3 ef d-0 ef af, f' ef
  g, ff'-2 ef d-0 ef g, ff'-2 ef-1 |
  af,16 f'! ef d ef c-1 bf'-4\< af
  b,-1 af'-3 g fs g d-1 c'-4 b | \pageBreak

% PAGE TWO

% bar 34
  ef,16-1_II d'-4 c b c g-1 f'-4 ef
  fs,-1 ef'-3 d cs d a!-1 g'-4 fs |
  b,16-1_II af'-3_I  g fs g \clef "tenor" d\thumb c'-2 b
  ef,-1 d' c b c\! \clef "treble" g-1_II f'-3_I ef | \break

% bar 36
  fs,16\f-1_II ef'-3_I d cs-1 d f,-2_II d'-3_I b-1
  e,-1_II d'-3 c b c ef,-2_II c'-3_I a | \clef "tenor"
  bf16-2 d,-1_II cs-1 a'-1_I af-2 c,-1_II a-1 fs'-3_I
  g-4 b,-3_II as-2 fs'-3_I f-2 a,-1_II fs-1 ds'-3 | \break

% bar 38
  e16-4 gs,-3 g-2 ef'-3 d-2 fs,-1 f-2 b-1
  c e, df'-3 b! c e, df' b |
  c16 e, df' b c e, df' b
  c\> e, d' c b c bf g | \clef "bass"
  f16\mf a d c b c a f
  e g d' c b! c g e | \break

% bar 41
  d16 f c' b a b f d
  e g d' c b c bf g |
  f16 a d c b c a f
  a c g' f e f c a |
  gs-1 b-4 f'-3 e ds e b gs
  g-1 bf f' ef d ef bf g | \break

% bar 44
  fs16-1 a ef'-3 d cs d a fs
  f-1 af ef'-4 df c df af f |
  e16-1 g df'-3 c b c g e
  d!-0 f c' b a-0 b f d |
  c16\< d e f g a bf! c
  d c bf g e c bf\! g | \break
  
% bar 47
  f16\< a c e f a d\! c
  b-1\> c a f d-0 f d bf! |
  f16\< a c e f a d\! c
  b-1\> c a f d-0 f d bf!\! |
  f16 a c e f a d c
  b-1 c a f e'-1 g f a,-0 | \break

% bar 50
  gs16-1 f'-3 e-2 g,-1 fs-1 ef'-3 d-2 f,-1
  e-1 d'-4 c ef,-1 d!-0 c'-2 bf df,-2_III |
  c16-1 bf'-4 a c,-1 b-1 a'-4 g bf,-1
  a-1 g'-4 f a, g-0 f'-2 e g, |
  f16\< a c e f a d\! c
  b-1\> c a f d-0 f d bf! | \break

% bar 53
  f16\< a c e f a d\! c
  b-1\> c a f d-0 f d bf!\! |
  f16 a c e f a d c
  b-1 c a f e'-1 g f a,-0 |
  gs16-1 f'-3 e-2 g,-1 fs ef'-3 d-2 f,-1
  e-1 d'-4 c ef, d-0 c'-2 bf-1 ef,-2 | \break

% bar 56
  c16-1 bf'-4 a c,-1 b-1 a'-4 g bf,-1
  a-1 g'-4 f a, g-0 f'-2 e g, |
  f16 a c e f\< a d cs-3
  c-3 b bf a-0 ef'-4_> d df c | \break

% bar 58
  d!16-4_>\! cs c b f'-4_> e! ef d
  e-4_> ds d cs g'-4_> fs f e |
  f16-4_> e ef d af'-4_> g gf e
  f-4_> e ef d af'-4_> g gf f-1 |

% bar 60
  c16\f-2 a'!-4 g-2 f-1 e!-1 g-4 f a,-0
  d-4 c bf g e c bf g |
  f a c e f a d c
  b c a f d f d bf |
  f a c e f a d c
  b c a f d f d bf | \break

% bar 64
  f a c e f a d c
  a, c e f a-0 c-2 g'-4 f |
  e16-1 g f a,-0 b-1_> d c f,-2
  gs-1_> bf-3 a c,-1 e-1_>_II g f a,_II |
  f1 | \bar "|." \pageBreak
% end
}

\score {
  \new Staff \six
  \header {
    title = "6"
  }
  \layout {
    #(layout-set-staff-size 19) % janky fix for long lines
  }
  \midi {}
}

% Local Variables:
% compile-command: "make -B 06.pdf"
% End:
