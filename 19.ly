\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

nineteen = \relative c {
  \tempo "Allegro"
  \key ef \major
  \time 4/4
  \clef "bass"
  \autoPageBreaksOff

% All but last bar is triplets
  \omit TupletNumber
  \override TupletBracket.bracket-visibility = ##f
  \tuplet 3/2 {

% bar 1
    r8 bf-3\upbow d-0 ef-1 bf d ef bf d ef bf'-4 g |
    ef'8 g, bf ef,-1 g bf, ef g, bf ef, g bf |
    af8 c e f c e f c e f-1 c' af-4 | \break

% bar 4
    f'8-2 af,-1 c f,-1 af c, f af,-1 c f, af cf-3 |
    bf8\< d f-1 af f af d-3-> f, af df-2-> f, af |
    c8-1->\> f,-1 af cf-1-> f,-1 af bf-1 f-1 af d,-0 f bf, |
    ef8\p bf-3 d-0 ef-1 bf d ef bf d ef bf'-4 g | \break

% bar 8
    ef'8\< g,-1 bf ef bf ef g-1 ef-3 g-1\! bf-3\> g df-2_II |
    c8-1\p c, e f c e f c e f-1 c' af\< |
    f'8-2 af,-1 c f-1 c-3 f af-4 f af-1 cf-3 af ef-2_II\! |
    d8-2 bf'-3_I df,-2_II c-1 af'-2_I cf,-2_II
    bf-1 g'-3 a,-2 af-1 f'-3 g,-2 | \break

% bar 12
    gf8-1\< ef'-3 f,-1 gf-2 ef'-4 f, gf ef' f, gf\! ef'\> a,,-1 |
    bf8-2 d'-4\< f, gf-3 d'-4 f, gf d' f, gf\! d'\> bf, |
    a8-1 ef''-4\< f,-1 gf ef' f, gf ef' f, gf ef'\> a,,-1 |
    bf8-2 d'-4 f,-2\< gf-3 d' f, gf d' f, gf\> d' bf, | \break

% bar 16
    b8-1 f''-4\< g,-1 af f' g, af f' g, af f'\> bf,,-1 |
    c8-2 ef'-3\< fs, g ef' fs, g ef' fs, g\> ef' c, |
    b8-1 f''-4\< g,-1 af f' g, af f' g, af\> f' bf,,-1 |
    c8-2 ef'-3\< fs,-1 g-2 ef' fs, g ef' fs, g\> ef' c, | \break

% bar 20
    cs8-1 g''-4\< a,-1 bf g' c,, d-2 fs'-4 gs,-1 a fs' d,-2 |
    b8-1 f''!-4 g,!-1 af f' b,,-1 c!-2 e'-4 fs, g e' c, |
    a8-1 ef''-4 f,!-1 gf-2 ef' a,,-1 bf-2 d'-4 f,-2 gf-3 d' bf,-2 | \break

% bar 23
    a8-1 ef''-4 f,!-1 gf-2 ef' a,,-1 bf-2 d'-4 f,-2 gf-3\> d' bf, |
    gf8-4\p df'-4 f gf-2 df f gf df f gf df'-4_II bf |
    gf'8-2 bf,-1_II df-4 gf,-4_III bf-1 df,-4_III
    gf-2 bf, df gf,-4_IV bf f-3 | \break

% bar 26
    e8-2\< df'-4 fs-2 g!-3 df fs g df fs g df'-4_II bf-1 |
    g'!8-3 bf,-1_II df-4 g,!-1 bf-4 e,-1 g-4\> bf,-1 df g,!-0 bf-1 e,-2\! |
    f8-3\< f'-1 a-0 bf-1 cs-3 d e-1 g f d-4 bf g | \break

% bar 29
    f8 d-0 cs'-3 d e-1 f \clef "tenor"
    a-1 c bf g\thumb f-3 d-1 |
    bf8-3_II f\thumb d'-1_II e-1 f-2 a\thumb \clef "treble"
    bf\thumb ef-3 d-2 bf g-2_II f-1 |
    d8\thumb-0\f c'-2_I bf g-3_II f-2 d\thumb
    bf\thumb g'-1 f d-2 bf\thumb g-2-0_III | \clef "bass" \break

% bar 32
    f8-1\> ef'-3_II d-2 bf\thumb g-2_III f-1 d-0_II c'-4 bf g-4 f d |
    bf8\mf g' gf f d-0 a-1 bf g' gf f d f,-4 |
    bf8 g' gf f d a bf g' gf f d f, |
    bf8-1 af'-4 f ef-1-> af-4 f d-0-> af'-4 f cf-2-> af'-4 f | \pageBreak

% PAGE TWO

% bar 36
    bf,8-1 af'-4 f ef-1-> af-4 f d-> af' f cf-2-> af' f |
    bf,8 af'-4 f d-0 af' f bf,\< af' f d f d |
    bf8 c'-3 bf af g-4 f ef d c bf af f\! |
    ef8\mf bf'-2 d-0 ef-1 bf-2 d ef bf-2 d ef bf'-4 g | \break

% bar 40
    ef'8 g, bf ef,-1 g bf, ef g, bf ef, g bf |
    af8 c e f c e f c e f-1 c' af-4 |
    f'8-2 af,-1 c f,-1 af c, f af,-1 c f, af cf-3 |
    bf8\< d f-1 af f af d-3-> f, af df-2-> f, af | \break

% bar 44
    c8-1-> f, af\! cf-1->\> f,-1 af bf-1 f-1 af d,-0 f-2 bf,\! |
    ef8\p bf d ef bf d ef bf d ef bf'-4 g |
    ef'8\< g, bf ef bf ef g-1 ef-3 g-1 bf-3 g\> df-2 |
    c8-1\p c, e f c e f c e f-1 c' af | \break

% bar 48
    f'8-2\< af, c f-1 c-3 f af-4 f-1 af-1 cf-3 af ef-2\! |
    d8-2 bf' df,-2 c af' cf,-2 bf-1 g'-3 a,-2 af\< f'-3 g,-2 |
    gf8-1 ef'-3 f,-1 gf-2 ef'-4 f, gf ef' f, gf\! ef'\> a,,-1 |
    bf8 d'-4\< f, gf-3 d'-4 f, gf d' f, gf\! d'\> bf, | \break

% bar 52
    a8 ef''-4\! f,-1 gf ef' f, gf ef' f, gf\! ef'\> a,,-1 |
    bf8 d'-4\! f,-2\< gf-3 d'-4 f, gf d' f, gf d' bf, |
    b8-1 f''-4 g,-1 af-2 f' g, af f' g, af f' b,,-1 |
    c8-2 ef' fs, g ef' fs, g ef' fs, g\! ef'\> c, | \break

% bar 56
    b8-1 f''-4\! g,\< af f' g, af f' g, af f' b,,-1\! |
    c8-2\< ef'-3 fs,-1 g-2 ef' fs, g ef' fs, g\! ef'\> c, |
    cs8-1 g''-4\! a,-1\< bf g' cs,,-1 d-2 fs'-4 gs,-1 a fs' d,-2 |
    b8-1 f''!-4 g,!-1 af f' b,,-1 c!-2 e'-4 fs, g e' c,\! | \break

% Lines get too long here, so just use automatic breaks
% bar 60
    a8-1 ef''-4 f,-1 gf-2 ef' a,, bf!-2 d'-4 f,-2 gf-3 d' bf,-2 |
    a8-1 ef''-4 f,-1 gf-2 ef' a,,-1 bf!-2 d'-4 f,-2 gf-3 d' bf,-2\< |
    g8-0 ff'-1 df'-3 b-1 c af, a-1 gf'-2 ef'-4 cs-2 d-3 bf,!-1 |
    b8-1 af'-2 f'-4 d-1 ef-2 c,-1 cs-1 bf'-2 g' e-1 f ef,!-2_III |

% bar 64
    d8-1\f bf'-2_II af'-4 e-1 f cs-1 d a-0 bf g-3 af-4 fs-2\< |
    g8 d ef fs-3 g d ef a, c bf g ef\! |
    d8\f bf''-1_II af'_"I" e-1 f cs-1 d a-0 bf g-3 af fs |
    g8\< d-0 ef fs-3 g d ef a, c bf g-0 df\! |

% bar 68
    c8\f-0 c''-1_II bf'-3_I fs-1 g ds-1 e c-1 df b-1 c-1 g-3\< |
    af8-4 e-1 f-1 g af e-1 f b,-3 df-4 c-4 af f\! |
    c8\f-0 c''-1_II bf'-3_I fs-1 g ds-1 e\< c-1 df b-1 c-1 g-3 |
    af8 e-1 f-1 g af e-1\! f\> b,-3 df-4 c-4 af f\! |

% bar 72
    d!8 bf''-1\< c d-4 ef-1 f g-4 c,-2 d e-1 f-1 g |
    af8 d,-1 ef \clef "tenor" f-4 g-1 af bf ef,-1 f g-1 af bf-1 | \clef "treble"
    c8-2\f af\thumb bf c e,-1_II f-2 af\thumb_"I" c e,_"II"f af_"I" c |

% bar 75
    d8-2 bf\thumb c d fs,-1_II g bf\thumb d fs, g bf\thumb d |
    ef8-3 bf\thumb c-1 g-2 bf\thumb ef,\thumb g bf,-1_III d-3\< ef\thumb bf d |
    ef8\thumb\! bf'\thumb c-1 g-2 bf ef, g bf,-1_II d-3\<  ef\thumb bf d |
    ef8\f ef'-3 c bf\thumb g-2_II ef\thumb
    bf\thumb af'-2 g ef-3 c-1 bf\thumb | \clef "bass"

% bar 79
    g8\thumb_"II"\> f'-3 ef c-3 bf g ef-1 c'-2 bf g-4 ef c-4_III\! |
    bf8\mf\> af'-4 g-4 ef c bf g_0 f' ef c bf g |
    ef8\p f' ef c bf g ef f' ef c bf g |
  }
  ef1 \bar "|." \pageBreak
% end
}

\score {
  \new Staff \nineteen
  \header {
    title = "19"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 19.pdf"
% End:
