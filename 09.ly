\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

%% Commentary:
% Every note in this piece is a double-stop.  Therefore, I have
% created two parts: top and bottom, which are joined together in the
% \score block.  Because they are in the same voice, commands like
% \slurUp don't work so all placements must be explicit.  For
% consistency, I have chosen to declare all dynamics in the bottom
% part, while bowings and other miscellaneous markings are in the top
% part.

global = {
  \tempo "Andante sostenuto"
  \key ef \major
  \time 2/4
  \clef "treble"
  \partial 8
}

top = \relative c' {
  \global
  
  g'16\thumb\downbow af-1 |
% bar 1
  bf8\thumb c16-1 bf g8\thumb g16 af-1 |
  bf16\thumb bf c bf ef8 g,16\thumb af-1 |
  bf8\thumb c16-1 bf g8\thumb g16 af | \break

% bar 4
  bf16\thumb bf c-1 bf ef8-3 c16-1 ef-3 |
  af,8-1 af16 c16-3 f,8-1 \clef "tenor" f16\thumb^( af-3 |
  g16-3 fs-3 g-4 f-4) ef8-2\upbow \clef "bass" d16-2\downbow^( ef-2 | \break

% bar 7
  b8-2) b16-2^( c-3 gf8-3) f16-2^( c'-4 |
  bf16) bf,-2\upbow c8-4 bf\fermata^\markup{\italic "longa"} bf'16-1\downbow^([ d-4] |
  f8-3) e16-3^( ef-4 d8-3) bf16-1^( d-4 | \clef "tenor" \break

% bar 10
  af'8-4) g16-3^( fs-4 f8-3) d16-3^( ef-2 |
  f8-4) ef16-2^( f-4 fs8-3) es16-3^( fs-3 |
  g!8-4) fs16-3^( f-4 ef8-2) ef16\thumb^( f-1 | \break
  
% bar 13
  f8\thumb) f16^( g-1 g8\thumb) g16^( af |
  af8\thumb) af16^( bf-1 bf8\thumb) bf16^( ef-3 |
  c8-1) a16\thumb^( d bf8-1) g16\thumb^( c-3 |
  af!8-1) f16\thumb^( bf-3 g8-1)
  \new Voice {
    \stemUp
    ef8-3^D^( | \break

% bar 17
    f8\thumb^A) f-3^( g\thumb)
  }
  af16-1 c-3 | \clef "treble"
  bf16-1 d-3 bf d c-1 ef-3 ef c-1 |
  af16-3^D  f-1 d'-3^A bf-1 g-3^D ef-1 \clef "tenor" c'[^A^( af-1] | \break

% bar 20
  f16-3^D d-1) bf'-3^A^( g-1 ef-3^D c-1) \clef "bass" gf16.-3^( f32-2) |
  f4.\fermata r8 |
  a16-0^( ef'-4 a, ef' a, gf'-3) f8-2\upbow
  a,16-0^( ef'-4 a, ef' a, gf'-3) f8-2 | \pageBreak

% PAGE TWO

% bar 24
  a,16-0^( ef'-3 a, ef' a, bf'-3) a8-3 |
  a,16-0^( ef'-3 a, ef' a, bf'-3) a8-3 |
  g16-2^( af-3 g af) g^( df-3 c8-2) | \break

% bar 27
  f16-3^( gf-4 f gf) f^( cf-2 bf8-1) |
  \new Voice {
    \stemUp
    bf16-1^( g'8 f16-4 ef-2) ef8-4^( d16-3) |
    c16-1^( af'8-4 g16-3 f-4) f8^( ef16-2) | \break
  }

% bar 30
  d16-2^( ef-3 d-2 b-1 c-2 df-3 c a-0) |
  bf4-1^( \clef "tenor" af'8-1\fermata \clef "treble" g16\thumb\downbow af-1
  bf8\thumb c16-1 bf g8\thumb g16 af-1 |
  bf16\thumb bf c bf ef8-3 g,16\thumb af-1 | \break

% bar 34
  bf8\thumb c16 bf g8\thumb g16 af |
  bf16\thumb bf c bf ef8-3 c16-1 ef-3 |
  af,8-1 af16 c-3 f,8-1 \clef "tenor" f16\thumb^( af-3 |
  g16-3 fs-3 g-4 f-3) ef8-2\upbow \clef "bass" d16-2^( ef-3 | \break

% bar 38
  b8-2) b16-2^( c-3 gf8-3) f16^( c'-4 |
  bf16-2) bf, c8 bf8\fermata^\markup{\italic longa} bf'16-1\downbow^( d-4 |
  f8-4) e16-3^( ef-4 d8-3) bf16-1^( d-4 | \clef "tenor"
  af'!8-4) g16-3( fs-4 f8-3) d16-3^( ef-2 | \break

% bar 42
  f8-4) ef16-2^( f fs8-3) es16-3^( fs-3 |
  g!8-4) fs16-3^( f-4 ef8-2) ef16\thumb\downbow^( f-1 |
  f8\thumb) f16^( g-1 g8\thumb) g16^( af-1 |
  af8\thumb) af16^( bf-1 bf8\thumb) bf16^( ef | \break

% bar 46
  c8-1) a16\thumb^( d bf8-1) g16\thumb^( c-3 |
  af8-1) f16\thumb^( bf-3 g8-1)
  \new Voice {
    \stemUp
    ef8-3^D^( |
    f8^A\thumb) f-3^D^( g^A\thumb) af^D-3^( | \clef "treble"
    bf8^A\thumb) af^D-3^( bf^A\thumb) bf16^( ef-3 | \break
    
% bar 50
    c8-1 a16\thumb) d-3^( bf8-1 g16\thumb) c-3^( |
    af!8-1 f16\thumb) bf-3^( g8-1\upbow) \clef "bass"
  }
  df16\downbow^( ef-4 |
  c8-1) af16-2\upbow^( bf-4 g8-1) df16-2 ef-4 |
  c8-1 af4-1 bf8-2 |
  g2-0\fermata \bar "|." \pageBreak
}

bottom = \relative c' {
  \global

  ef16_2_( f_3 |
% bar 1
  g8_2) af16_3_( g ef8_2) ef16_( f_3 |
  g16) g_( af g ef8) ef16_2_( f_3 |
  g8) af16_3_( g ef8) ef16_( f | \break

% bar 4
  g16_2) g_( af_3 g ef8_\thumb) ef16_( g_2 |
  c,8_\thumb) c16_( ef_2 af,8_\thumb) \clef "tenor" af16_1 c_2 |
  bf16_1 a_1 bf_2 af_2 g8 \clef "bass" fs16_1 g_2 | \break

% bar 7
  d8 d16-> ef bf8_2 a16_1 ef'_2 |
  d16\p d,_1_( f8_4 d) d'16\mf-0 f_2 |
  af8_2 g16_1 gf_2 f8_1\< d16_0 f_2 | \clef "tenor" \break

% bar 10
  c'8_3 bf16_1 a_2\! af8_1 f16_1\> g_1 |
  af8_2 g16_1 af_2 a8 gs16_1 a_1 |
  bf8_2 a16_1 af_2 g8_1\! c16_2\p af_\thumb | \break

% bar 13
  d8_2 d16 bf_\thumb ef8\< ef16 c_\thumb |
  f8_2 f16 d_0_\thumb g8_2\! g16 ef_\thumb\> |
  af8_3 fs16_2 d_\thumb g8_3 ef16_2 c_\thumb |
  f!8_3 d16_2 bf_\thumb ef8\mf_3 ef,16_G_\thumb_( g | \break

% bar 17
  bf16_D_\thumb d_2) f,_G_\thumb_( af_2 c_\thumb_"D" ef_2) c_\thumb_( ef_2 | \clef "treble"
  d16_\thumb f_2) d_( f ef_\thumb g_2) g_( ef_\thumb |
  c16_G_2 af_\thumb) f'_2_D_( d_\thumb bf_2_G g) \clef "tenor" ef'_2_D c_\thumb | \break

% bar 20
  af16_2_G f_\thumb d'_2_D bf_\thumb g_2 ef_\thumb \clef "bass" bf16._2 a32_1 |
  a4. r8 |
  f'16_1\p gf_2 f gf f bf_2 a8_1 |
  f16_1 gf_2 f gf f bf_2 a8_1 | \pageBreak

% PAGE TWO

% bar 24
  fs16_1 g!_2 fs g fs d'_2 c8_1 |
  fs,16\< g! fs g fs d'_2 c8_1 |
  b16_1\f d,_0 b' d, b' f_2 e8_1 | \break

% bar 27
  a16_2 af_1 a\> af a ef_1 d8_0 |
  <g,_0 ef'>16\!_( bf'_2 a_1 af_2 g_1) gf_2_( f8_1) |
  af!16_4_( c_3 b_2 bf_1 a_3) af_2_( g8_1) | \break

% bar 30
  fs16\>_1 g_2 fs_1 f_2 e_1 f_2 e_1 ef_1 |
  d4_0 \clef "tenor" d'8_2)_\markup{\italic "longa"} \clef "treble" ef16_2\mf_( f_3 |
  g8_2) af16_3_( g ef8_\thumb) ef16_2 f_3_( |
  g16) g_( af g ef8_\thumb) ef16_2_( ef_3 | \break

% bar 34
  g8_2) af16_3_( g ef8_2) ef16_( f_3 |
  g16_2) g_( af g ef8) ef16_\thumb_( g_2 |
  c,8_\thumb) c16_( ef_2 af,8) \clef "tenor" af16_1 c_2 |
  bf16_1 a_1 bf_2 af_2 g8_1 \clef "bass" fs16_2 g_2 | \break

% bar 38
  d8_0 d16\> ef_1 bf8_2 a16_1 ef'_2 |
  d16_1 d,\pp_( f8 d8) d'16_0\mf f_2 |
  af!8_2 g16_1 gf_2\< f8_1 d16_0 f_2 | \clef "tenor"
  c'8_3 bf16_1 a_2 af8_1 f!16_1 g_1 | \break

% bar 42
  af8\!_2 g16_1 af a8 gs16_1 g_1 |
  bf8_2 a16_1\> af_2 g8_1 c16_2\p af_\thumb |
  d8_2\< d16 bf_\thumb ef8_2 ef16 c_\thumb |
  f8_2 f16 d_\thumb_0 g8_2\! g16 ef_\thumb | \break

% bar 46
  af8 fs16_2\> d_\thumb g8_3 ef16_2 c_\thumb |
  f8_3 d16_2 bf_\thumb ef8_3 ef,16_G_\thumb_( g_2 |
  \stemDown % while the top part is in another voice
  bf16_D_\thumb\! d_2) f,_D_\thumb_( af_2 c_"D"_\thumb ef_2) af,_G_\thumb_( c_2 | \clef "treble"
  ef16_D_\thumb g_2) af,_G_\thumb_( c_2 ef_"D"_\thumb g_2) g ef_\thumb | \break

% bar 50
  af16_3 g_2 fs_2 d_\thumb g_3 f_2 ef_2 c_\thumb |
  f16_3 ef_2 d_2 bf_\thumb ef8_3 \clef "bass" \stemNeutral f,16\p_1 g_3 |
  af8_4 c,16_1\> d!_3 ef8_4 f,16_1_( g_3 |
  af8_4) c,4_( d!8_1\!) |
  ef2\pp_2 \bar "|." \pageBreak
% end
}

\score {
  \new Voice << \top \bottom >>
  \header {
    title = "9"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 09.pdf"
% End:
