\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

seven = \relative c' {
  \tempo "Lebhaft"
  \key a \major
  \time 4/4
  \clef "bass"
  \override TupletBracket.bracket-visibility = ##f


  \tuplet 3/2 { % upbeat - triplet number is shown
    \partial 4.
    fs8-4\f( es e |
  }

  \omit TupletNumber % no triplet number for the rest of the piece
  \tuplet 3/2 {
% bar 1
    ds8-1 fs a,-0 c-2 b a
    gs-1 b-4 d,-0 f-2 e d |
    cs8-4) e-1( a-0 cs-3 b a
    d-4 cs b e-4 d cs) | \break
    
% bar 3
    fs8-4 es e ds c-3 b
    bf a-0 c-3 a fs-4 ds |
    e8-2 gs-1 b \clef "tenor" e-2 gs-1 b-3
  } % no triplet for half a bar
  e4.^0_0_3\fermata\upbow r8 | \break

  \tuplet 3/2 {
% bar 5
    e,8\mf-0\downbow( fs-1 cs-2 e fs gs
    a\thumb-0 cs) e,-1( fs-2 gs-3 a | \clef "treble"
    bf8-1 d-3) f,-2( a\thumb-0 b-1 c-2
    d-1 f-3) f,\thumb( b-3 d f | \break

% bar 7
    e!8-2) fs-3( ds-1 e gs-1 b!-2
    a) b( gs a fs-3 d-1 |
    cs8-2) d-3( bs-1 cs e-1 gs
    fs-2) gs( e fs d-3 b | \break

% bar 9
    a8-2) b-3( gs-1 a cs-1 e
    ds-2 c-2 a\thumb-0 f-2 a d-3 |
    cs8 a\thumb-0) e-1( ds-1 a' c-2
    cs-2 a e-1 f-2 a d-3 | \break

% bar 11
    cs8 a) e( ds a' c
    cs a e f a d |
    cs8-2 a\thumb-0) \clef "tenor" es-1_D fs-2 d-2 b'-3
    a-2 fs\thumb cs-1_D d-2 b-2 gs'-3 | \break

% bar 13
    fs8-2 d\thumb( as-1_D b-2 gs-2 e'-3 \clef "bass"
    a,!-0 fs-2 ds'-4 a-0 f-2 d'-4 |
    a8-0 fs-2 ds'-4 c-1 a-0 fs'-4
    d-1 \clef "tenor" a'-3 fs-1) \clef "treble"
  } % no triplet for 1 beat
  a'4^0_1\upbow \clef "bass" | \break
  
  \tuplet 3/2 {
% bar 15
    a,,8\p-0\rtoe( f-2 d'-4 c a-0 g'-4
    f-2 a,-0 e'-1 f a,-0 e'-2 |
    ef-1 a,-0 d-1 ef-2 a,-0 d-2
    cs-1 a-0 bs-1 cs-2 a-0 c-2 | \pageBreak

% PAGE TWO

% bar 17
    b8 a-0) c-2( d-4 a-0 c-2
    b-1 a-0 c-2 b-1 a-0 a-.) |
    e8-1\rtoe( a-0 cs-1 e-4_\markup{\italic "gently sliding"} fs-1 cs-2 \clef "tenor"
    e\thumb fs-1 gs-2 a\thumb-0 cs-2 e,-1 | \clef "bass" \break

% bar 19
    f,8-2) bf-1( d-1 f-4 g-1 d-2 \clef "tenor"
    f!\thumb g!-1 a bf\thumb d-2 f,-1 |
    fs,8-3) b-1( ds-1 fs-4 gs-1 ds-2
    fs\thumb gs-1 as-2 \clef "treble" b\thumb ds-2 fs,-1 | \break

% bar 21
    g8) e\thumb( e'-3 c-1 g-2 e
    d-1_II c\thumb c'-3 g\thumb e-2 c\thumb | \clef "bass"
    a8-1_II) g\thumb( g'-3 e-1 c-3_II g\thumb
    f-2 e d'-4 c g e | \break

% bar 23
    c8-4\<) a( g-0 e d c
    e g b c e g-4) |
    c8\mf-4\rtoe( e-1 b-3 a-1 g'-4 b,-3
    c e b a g' b,) | \break

% bar 25
    c8( e b a g' b,-1
    c e b a g' b,) |
    c8-4\>\rtoe( bf g'-4 fs-3 e-1 b-2
    a-2 gs f'-3 e-2 d-1 af-2 | \break

% bar 27
    g8-2 fs ef'-3 d-2 c-1 fs,-2
    f-2 e) cs'-3\upbow( c-2 b-1 a-0) |
    e8\p( a-0 cs!-1 \clef "tenor" e-4 a\thumb-0 cs-1 \clef "treble"
    e-0-2 a\thumb-0 cs-0-1 e-0-2 cs-0-1 a'-0-3 | \break

% bar 29
    e8-0-2 cs-0-2 a-0-1 fs-0-2_II e\thumb-0_I cs'-0-2
    a-0-1 fs-1 e\thumb-0 cs-2_II a\thumb-0 fs'-1_I |
    e8\thumb-0) cs-2( a\thumb-0 fs-2_II e-0 cs'-2_I
    b a\thumb-0 fs-1 e\thumb cs-2_II a'-3_I | \clef "tenor" \break

% bar 31
    fs8-1) e\thumb( cs-2_II a\thumb e-1_III cs'-2_II
    fs-1 e\thumb cs-2 a\thumb e-1 cs'-2 |
    fs8-1 e\thumb cs-2 a\thumb e-1 cs'-2
    fs e cs a e cs' | \break

% bar 33
    fs8-1) e\thumb( cs-2 b-1 a\thumb cs\thumb
    b'-2 a-1 e-2 d cs\thumb e\thumb | \clef "treble"
    d'8-2) cs( a-3 fs-1 e\thumb a\thumb-0
    fs'-1 e\thumb-0 cs-2 b-1 a\thumb-0 e'\thumb-0) |
  } % end triplets
  a1-0-3\upbow |
  a'1_3^0\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \seven
  \header {
    title = "7"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 07.pdf"
% End:
