\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

%% Commentary:
% This is another annoying double stop filled etude.  It is split into
% two parts combined into a single voice.  The parts are sometimes
% split into separate voices where needed.

global = \relative c' {
  \tempo "Con brio"
  \key c \minor
  \time 4/4
  \clef "bass"
  \autoPageBreaksOff
}

top = \relative c' {
  \global

% bar 1
  s8 c16-3( d-3 ef4-4->) s8 c16( d ef4->) |
  s8 c16-3( d-3 ef-4 f-3 fs-4 g-3
  af-4) g-3( fs-3 g-4 f-4) e-3( d-3 e-3) |
  s8 f16-4( g-3 af4-4->) s8 f16( g af4->) | \break

% bar 4
  s8 af16-4( g-3 f-4 ef-2 d-3 c-3)
  b-2( c-3 cs-4 d-3 ef-4) d-3( c-3 b-2) |
  s8 c16-3( d-3 ef4-4->) s8 c16( d ef4->) |
  s8 c16-2( d-4 e-3 f-4 g-4 f-2
  ef8-1) bf'!16-3\downbow bf( af-2 g-4 f-4 e-3) | \break

% bar 7
  s8^\markup{\italic "a tempo"} f16-4( g-3 af4-4->)
  s8 d,!16( ef-2 f4-4->) |
  s8 ef16-2( f-4 g4-4->) s8 c,16-3( d-3 ef4-4->) |
  s8 d16-3( ef-2 f4-4->) s8 b,16-2( c-3 d4-3->) |
  c16-3 c( g'-4) g( c,-3) c\(( g-4) g-.\) c,4.\fermata r8 |
  \key c \major \clef "tenor" \bar "||" \break
  \tempo "Andante quasi Adagio"

% bar 11
  e'8\thumb( g-2 a-3 g-2 e) g c,\thumb e-2 | \clef "bass"
  \new Voice {
    \stemUp
    a,8-1 c4^( b8) <e, c'>4.\upbow s8 |
    a8\downbow c f4-2
  }
  e8-1 d-2( c-1 bf-1 |
  a8) g!-0-3 f-4 g,-0 a4.-1 r8 | \break

% bar 15
  a8-1\downbow( a'-0 d-4 fs-3) \clef "tenor"
  a\thumb-0\upbow( d-3)
  \new Voice {
    \stemUp
    <g,-3 b-1>4\downbow | \clef "bass"
  }
  g,,8\upbow( g'-0-3 c-2^I) e-3\downbow( \clef "tenor"
  g\thumb) c-3\upbow( a4-1) | \clef "treble"
  \new Voice {
    \stemUp
    a4\downbow\thumb-0^( f'8-3) a,\upbow^( b\thumb)
    b\downbow^( g'-3) b,\upbow\thumb^( | \break

% bar 18
    c8\thumb) c\downbow^( a'-3) g-3 f-2^(
    \clef "bass" e,-4) e( d) | \clef "tenor"
  }
  e8\thumb( g-2 a-3 g-2 e\thumb) g-2( c,\thumb e-2 | \clef "bass"
  a,8)
  \new Voice {
    \stemUp
    c4\downbow^( b8)
    <e, c'>4.\fermata
  }
  r8 | \bar "||" \key c \minor \break

% bar 21
  s8 c'16-3( d-3 ef4-4->) s8 c16( d ef4->) |
  s8 c16-3( d-3 ef-4 f-3 fs-4 g-3
  af-4) g-3( fs-3 g-4 f-4) e-3( d-3 e-3) | \break

% bar 23
  s8 f16-4( g-3 af4-4->) s8 f16( g af4->) |
  s8 af16-4( g-3 f-4 ef-2 d-3 c-3)
  b-2( c-3 cs-4 d-3 ef-4) d-3( c-3 b-2) |
  s8 c16-3( d-3 ef4-4->) s8 c16( d ef4->) | \break

% bar 26
  s8 c16-2( d-4 e-3 f-4 g-4 f-2
  ef8-1) bf'!16-3\downbow bf( af-2 g-4 f-4 e-3) |
  s8 f16-4( g-3 af4-4->) s8 d,!16( ef-2 f4-4->) |
  s8 ef16-2( f-4 g4-4->) s8 c,16-3( d-3 ef4-4->) | \break

% bar 29
  s8 d16-3( ef-2 f4-4->) s8 b,16-2( c-3 d4-3->) |
  c16-3 c( g'-4) g( c,-3) c\(( g-4) g-.\) c,4.\fermata r8 | \bar "||" \pageBreak
% end
}

bottom = \relative c, {
  \global
  \stemDown % Stem down for first couple of lines

% bar 1
  c8\f ef'16_1 f_1 g4_3 c,,8 ef'16 f g4 |
  c,,8 ef'16_1 f_1 g_3 af_1 a_2 bf_1
  c_3 bf_1 a_1 bf_2 af_2 g_1 f_1 g_1 |
  f,8-1 af'16_2 bf_1 c4_3 f,,8-1 af'16 bf c4 | \break

% bar 4
  f,,8-1 c''16_3 bf_1 af_2 g_1 f_1 ef_1
  <g, d'> ef'_1 e_2 f_1 g_3 f_1 ef_1 d_0 |
  c,8 ef'16_1 f_1 g4_3 c,,8 ef'16 f g4 |
  c,,8\< e'16_1 f_2 g_1 af_2 bf_2 b_3
  c8_4 df16_1_\markup{\italic "ritard."} df c_1 bf_2 af_2 g_1 | \break

% bar 7
  \stemNeutral
  f,8-1\f af'16_2 bf_1 c4_3 d,,!8-1 f'16_2 g_1 af4_2 |
  ef,8-1 g'16_1 af_2 bf4_2 c,,8-0 ef'16_1 f_1 g4_3 |
  f,8-3 f'16_1 g_1 af4_2 g,8_0 d'16_0 ef_1 f4_1 |
  <c, g' ef'>16 ef' b'_3 b ef,_1 ef g,_0 g c,4.\fermata r8 |
  \key c \major \clef "tenor" \bar "||" \break
  \tempo "Andante quasi Adagio"

% bar 11
  c''8_2_\markup{\dynamic p \italic "dolce"}
  b_1 a_\thumb b_1 c_2 b_1 a_2 g_1 | \clef "bass"
  \new Voice {
    \stemDown
    f8_2 e_1( <g, d'> g') r g, c, r |
    f'8_( e d_0 b'_3)
  }
  c_4 gs_3 a_4 e_2 |
  f8 c_0_3_( a_3 c,) f4._4 r8 | \break
  
% bar 15
  d8_1\p d'_0 fs_3 a_1 \clef "tenor" d_\thumb_0 fs_2
  \new Voice {
    \stemDown
    r8 g,,_"pizz." | \clef "bass"
  }
  c,8 c'_0_3 e_1_II g_1 \clef "tenor" c_\thumb e_2 f4_3 | \clef "treble"
  \new Voice {
    \stemDown
    f8_2( e_1 d_\thumb) f_2_( g_2) f_1_( e_\thumb) g_2_( | \break

% bar 18
    a8_2) g_1_( f_\thumb) g_\thumb a_1\>_(
    \clef "bass" fs,_1) g4\! | \clef "tenor"
  }
  c8\p_2 b_1 a_\thumb b_1 c_2 b_1 a_2 g_1 | \clef "bass"
  f8_2
  \new Voice {
    \stemDown
    e8_1( <g, d'> g') r8 g,[ c,]
  }
  r8 | \bar "||" \key c \minor \break

% bar 21
  \stemDown
  c8\f ef'16_1 f_1 g4_3 c,,8 ef'16 f g4 |
  c,,8 ef'16_1 f_1 g_3 af_1 a_2 bf_1
  c_3 bf_1 a_1 bf_2 af_2 g_1 f_1 g_1 | \break

% bar 23
  f,8-1 af'16_2 bf_1 c4_3 f,,8-1 af'16 bf c4 |
  f,,8-1 c''16_3 bf_1 af_2 g_1 f_1 ef_1
  <g, d'> ef'_1 e_2 f_1 g_3 f_1 ef_1 d_0 |
  c,8 ef'16_1 f_1 g4_3 c,,8 ef'16 f g4 | \break
  
% bar 26
  c,,8\< e'16_1 f_2 g_1 af_2 bf_2 b_3
  c8_4 df16_1_\markup{\italic "ritard."} df c_1 bf_2 af_2 g_1 |
  f,8-1\f af'16_2 bf_1 c4_3 d,,!8-1 f'16_2 g_1 af4_2 |
  ef,8-1 g'16_1 af_2 bf4_2 c,,8-0 ef'16_1 f_1 g4_3 | \break

% bar 29
  \stemNeutral
  f,8-3 f'16_1 g_1 af4_2 g,8_0 d'16_0 ef_1 f4_1 |
  <c, g' ef'>16 ef' b'_3 b ef,_1 ef g,_0 g c,4.\fermata r8 | \bar "|." \pageBreak
% end
}

\score {
  \new Voice << \top \bottom >>
  \header {
    title = "17"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 17.pdf"
% End:
