\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

three = \relative c' {
  \key bf \minor
  \time 4/4
  \tempo "Andante"
  \clef "bass"

  df16\mf-4( a-0 bf-1 f-3 gf-4 e-2 f-3 bf
  df) a( bf f gf e f bf |
  df16) a-0( bf f'-4 ef df-1 c-3 bf
  a) ef-1( gf-2 f af-4 gf f-1 c-2) | \break
  
% bar 3
  df16( bf af'-4 g-3 gf-2 c,-1 bf'-4 bff-3
  af-2) f'-4( c-1 df ef df af f |
  gf16) d-0( ef-1 c'-3_D bf-1 gf-4_G ef-1 bff-2~
  bff) g-0( af-1 f'-4 ef-2 c-1 af-4 gf) | \break
  
% bar 5
  f16-1\<( ef'-4 c df bf'16.->-4 bff32 af8)
  g,16-0( f'-3 d ef c'16.-4 a32 bf8) |
  a,16-1( gf'-3 e f df'16.->-4 b32-2 c8-3)
  b,16-1( af'-3 fs g ef'16.->-3 cs32 d8) | \break

% bar 7
  c,16-1( bf' g af) f'16.->-4\upbow( ff32 ef8)
  d,16-1( cf'-3 a bf) gf'!16.->\upbow( e32 f8) |
  ef,16-2\f( cf'-3 a-1 bf) af'16.->_4\upbow( g32_3 gf8_2)
  ef,16( cf' a bf) af'16.->\upbow( g32 gf8~ | \break

% bar 9
  gf16.) f32-2( e8-1~ e16.) ef32-2( d8-1~
  d16.\>) df32-2( c8-1~ c16.) df32( c8) |
  df16\mf-2( a-0 bf-1 f-3 gf-4 e-2 f-3 bf  
  df) a( bf f gf e f bf | \break

% bar 11
  df16) a-0( bf f'-4 ef df-1 c-3 bf
  a) ef-1( gf-2 f af-4 gf f-1 c-2) |  
  df16( bf af'-4 g-3 gf-2 c,-1 bf'-4 bff-3
  af-2) f'-4( c-1 df ef df af f | \break
  
% bar 13
  gf16) d-0( ef-1 c'-3_D bf-1 gf-4_G ef-1 bff-2~
  bff) g-0( af-1 f'-4 ef-2 c-1 af-4 gf) |
  f16-1( ef'-4 c df bf'16.->-4 bff32 af8)
  g,16-0\<( f'-3 d ef c'16.-4 a32 bf8) | \pageBreak
  
% PAGE TWO

% bar 15
  a,16-1( gf'-3 e f df'16.->-4 b32-2 c8-3)
  b,16-1( af'-3 fs g ef'16.->-3 cs32 d8) |
  c,16-1( bf' g af) f'16.->-4\upbow( ff32 ef8)
  d,16-1( cf'-3 a bf) gf'!16.->\upbow( e32 f8) | \break
  
% bar 17
  ef,16-2\f( cf'-3 a-1 bf) af'16.->_4\upbow( g32_3 gf8_2)
  ef,16( cf' a bf) af'16.->\upbow( g32 gf8~ |
  gf16.) f32-2( e8-1~ e16.) ef32-2( d8-1~
  d16.\>) df32-2( c8-1~ c16.) df32-4_D( c8) | \break
  
% bar 19
  df16-4\pp( c-3 bf f-3_G e-2 df'-4 b-2 c-3
  cf-4 bf-3 af-1 ef-3 d-1 cf'-3 a-1 bf-2 |
  a16-4) gs-3( fs-1 cs-3 bs-1 a'-4 fss-2 gs-3
  g-4 fs e b as g' es fs | \break

% bar 21
  gf16-4) f-3( ef-1 bf a gf' e f
  gf f ef bf a gf' e f |
  gf16-4) d-0( ef-1 df'-4_A c-3 a-0 gf'-4 e-2
  f-3 gf d-1 ef e-3 f b,-2 c-3) | \break

% bar 23
  df16\mf-4( a-0 bf-1 f-3 gf-4 e-2 f-3 bf
  df) a( bf f gf e f bf |
  df16) a-0( bf f'-4 ef df-1 c-3 bf
  a) bf( gf-4 f ef df-4 c bf) | \break

% bar 25
  a16-1\<( bf d,-1 ef e f) r8\!
  a16\<( bf d, ef e f) r8\! |
  a16-1\<( bf d,-1 ef e f a bf
  b-1 c) e,-1( f fs g b-1 c | \break

% bar 27
  c16-1 df!) f,-1( gf!  g-3 af c-1 df
  d-1 ef-2) g,!( af a-3 bf d-1_G ef\!) |
  e16-3( f a,-1 bf gf'!->-3_D f) a,-1_G( bf
  e-3 f a,-1 bf gf'-3_D f) a,-1_G bf | \break
  
% bar 29
  e16->-3_G( f) r8 r4
  e16->\downbow( f) r8 r4 |
  f2.-4\>\downbow( df4-4) |
  bf1\pp\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \three
  \header {
    title = "3"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 03.pdf"
% End:
