\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

eighteen = \relative c'' {
  \tempo "Allegro molto moderato"
  \key d \major
  \time 6/8
  \clef "treble"
  \override Fingering.staff-padding = #'()

  \stemUp
  \partial 8
  a16_0_\thumb\p fs_2 | \bar ".|:"
% bar 1
  b16_1 g_3 e_1 d' cs_2 e_0_4
  d_3 a_0_\thumb fs_2 d_0_\thumb a'_0_\thumb fs_2 |
  b16_1 g e d' cs e d a fs d a' fs | \break

% bar 3
  b16_1\< g_3 e_1 d'_3 cs_2 e_2 \stemNeutral
  d-1 fs-3 e d cs-2 b\! | \stemUp
  e16\> _0_4 cs_2 a_0_\thumb fs_2 d'_3 b
  a_0_\thumb e_1 a es_1 a fs_2 |
  b16_1\p g_3 e_1 d'_3 cs e_0_4 d_3 a_0_\thumb fs d a' fs | \break

% bar 6
  b16_1 g e d' cs e_4 d a fs d a' fs |
  b16\< g e d' cs_2 e_0_2 \stemNeutral
  d-1 fs-3 e-2 d cs-2 b\! | \stemUp
  e16_0_4\> cs_2 a_0_\thumb fs d'_3 b a e a es a fs | \break

% bar 9
  bf16_1\p g_3 e_1 d'_3 c bf
  a_0_\thumb c_2 a f_"II" d_0_\thumb c_3_III |
  b!16_2\< d_\thumb_0 f_2 a_0_\thumb g_3 f
  e_1 d'_3 c bf\> a_0_\thumb f |
  bf16_1\! g e d' c bf a_0_\thumb c_2 a f d_0_\thumb c_3 | \break

% bar 12
  \stemNeutral
  b!16-2\< d\thumb_"II" f-2 a\thumb_"I" b!-1 d-3
  f-1 a-3 f-1\! d-3_II b-1 a\thumb |
  b,!16-2_III\< ds-1_II fs-2 a\thumb-0_I b-1 ds-3
  fs-1 a-3 fs\!  ds-3_II\> b-1 a\thumb-0 | \clef "bass"
  e,16-1_II\mf a-0 cs-1 e-4 \clef "treble" a\thumb cs-2
  e\thumb-0 a-0-1 cs-0-2 e-0-3 cs-0-2 a-0-1 | \clef "bass" \break

% bar 15
  f,,-2_II a-0_I d-1 f-4\clef "treble" a\thumb-0 d-1
  f-3 a\thumb-0 d-1 f-3 d-1 a\thumb-0 | \clef "bass"
  fs,,!-3_II a-0_I ds-1 fs-4 \clef "treble" a\thumb-0 ds-1
  fs-3 a\thumb-0 ds-1 fs!-2 ds a\thumb-0 | \clef "bass"
  g,,16-2_II\mf a-0_I cs-1 e-4 \clef "treble" a\thumb-0 cs-1
  e-0-2 a\thumb-0 cs-0-1 e-0-2 a-0-3 e-0 | \break

% bar 18
  a16_3_0 e_2_0 a_0 e_0 a,_3_0 e_0_\thumb
  a e a e a,\thumb-0 e-1 |
  a16_\thumb\> e_1 a e a e a es_1 a es a fs_2 | \bar ":|."
  b16_1\p g e d' c e_0_4 d_3 a_0_\thumb fs_2 d_0_\thumb a'_0_\thumb fs_2 | \break

% bar 21
  b16 g e d' cs e d a fs d a' fs |
  b16\< g e d'_3 cs_2 e_2 d-1 fs-3 e-2 g-2 fs-1 a-3 |
  g16-2\mf b-4 a-3 fs-1 e\thumb-0 g-2 fs-1 d-3_II cs-2 e-4 d b | \break

% bar 24
  a16\thumb cs-2 b-1 g-3_III e-1 g-3
  fs-2 d\thumb-0 cs-3_IV e-1_III d\thumb-0 b-2_IV |
  a16-1 cs-3 e-1_III a\thumb-0 cs-2 e\thumb-0_I
  g-2 f-1 d-3_II bf-1 f-2_III d\thumb-0 |
  a16-1_IV cs e_"III" a\thumb-0_II cs e\thumb-0_I
  g-2 f-1 d-3_II bf-1 f-2_III d\thumb-0 | \break

% bar 27
  a16-1_IV cs-3 e-1_III a\thumb-0_II cs-2 e\thumb-0_I
  a-1-0_I e-0\thumb cs'-0-2 a-0-1 e'-0-2 cs-0-1 |
  a'16-0-3 e-0-2 cs-0-2 a-0-1 e'-0-3 cs-0-2
  a-0-1 e-0\thumb cs'-0-2 a-0-1 e-0\thumb cs-2_II |
  a'16-3_I e\thumb cs-2_II a\thumb e'\thumb_"I" cs-2_II
  a\thumb e-1_III cs'-2_II a\thumb e-1_III cs-3_IV |
  a4._"IV"\fermata~ a4 \bar "||" \clef "bass" \pageBreak

% PAGE TWO

% bar 31
  c,16-4_II\p a-1 | % upbeat
  d-0 bf-2 g-0 f'-2 e g f c a f c'-4 a-1 |
  d16-0 bf-2 g-0 f'-2 e g f c a f c' a | \break

% bar 33
  d16 bf g f' e g f a-0 g-4 f e d |
  g16 e c a f' d c g c gs-1 c a-1 |
  d16-0 bf g f' e g f c a f c' a | \break

% bar 36
  d16 bf g f' e g f c a f c' a |
  d16 bf g f' e g f a g f e d |
  g16 e c a f' d c g c gs c a | \break

% bar 39
  df16-4 bf bf g df' bf d-4\cresc b ef-4 c e-4 cs |
  f16-4 d d-4 b f'-4 d fs-4_II ds g-4 e gs-4 es | \break

% bar 41
  a16-4 fs fs-4 ds a'-4 fs bf-4 g b-4 gs c-4 a |
  cs-4 bf bf-4 g cs-4 bf d-4 b ef-4 c e-4 cs | \break

% bar 43
  f16-4 d d-4 b f'-4 d e-4 cs cs-4 as e'-4 cs |
  g'16-4 e e-4 cs g'-4 e fs-4 ds ds-4 c-1 fs-4 ds | \clef "tenor"
  a'16-3\f fs-1 ds\thumb fs\thumb \clef "treble" a-1 c-2
  ds-2 c-1 a\thumb c\thumb d-1 fs-2 | \break

% bar 46
  a16-2 fs-1 ds\thumb fs\thumb a-1 c-2 ds-2 c-1 a\thumb c\thumb ds-1 fs-2 |
  a16-0-4 a a a a a a,\thumb-0 a a a a a |
  \once \override Fingering.staff-padding = #1
  a,2.:16-0\thumb\> | \break

% bar 49
  a16\thumb gs-3_II a_"I" gs_"II" a gs a g-3 a g a fs-2 |
  b16-1\p g-3 e-1 d'-3 cs e-4 d a fs d a' fs |
  b16 g e d' cs e d a fs d a' fs | \break

% bar 52
  b16\< g e d' cs-2 e-2 d-1 fs-3 e-2 d-1 cs-2 b\! |
  e16-4\> cs a\thumb-0 fs d'-3 b a e a es a fs |
  b16\p g e d' cs e-0-4 d-3 a fs d a'\thumb-0 fs-2 | \break

% bar 55
  \override Fingering.staff-padding = #0.5 % Fingering looks cluttered here otherwise
  b16 g e d' cs e d a fs d a' fs |
  d'16-3\< cs-2 c-2 b a\thumb g-3_II
  fs e-1 ef-1 d\thumb cs!-3_III c-3\! |
  b16\mf\< g\thumb-0 d'\thumb-0 b-2 g'-3 d
  e\thumb_"II" b-1 gs'-2 e b'\thumb g\! | \pageBreak

% PAGE THREE

% bar 58
  e'16-3_I\f\< ds-2 d-2 cs b\thumb a-3_II
  gs-2 fs-1 f-1 e\thumb ds-3_III d-3 |
  cs16-2 a\thumb e'\thumb cs a'-3_II e
  fs\thumb cs-1_III as'-2_III fs cs'\thumb_"I" a |
  fs'16-3\f e-2 g-4 fs e-2 d-1 d-3 cs e-4 d cs b | \break

% bar 61
  b16-3 as cs-4\> b a g-1 fs\thumb g fs e-3_II d cs |
  d16-2\p b\thumb fs'\thumb d-2 b'-3 fs\thumb\<
  g\thumb d-1 b' g d'-4 b |
  fs-2_II\p d\thumb a'_"I" fs-2 d'\< a\thumb
  bf\thumb f-1 d'-2 bf f'-4 d | \break

% bar 64
  a16-2_II\p f\thumb c'\thumb_"I" a f'-3 c\thumb\<
  df\thumb af-1 f'-2 df af'-4 f |
  c16-2_II\p af\thumb ef'_I\thumb c\< af'-3 ef\thumb
  e\thumb b-1 gs'-2 e b'-3 g |
  fs16-1\mf gs-2 fs-1 ds-3_II b-1 gs'-2_I fs-3
  cs\thumb as-2_II fs\thumb cs'_"I" as_"II" | \break

% bar 67
  d!16-1_I e-2 d-1 a-2_I fs\thumb e'-1_I d-3 a\thumb fs-2 d\thumb_"II" a' fs |
  b16-1 cs b fs d\thumb-0 cs'-2_I b-3 fs\thumb d-2_II b\thumb fs'\thumb d |
  g16-1\< d-2 b\thumb d g-1 b-3 a\thumb-0 fs d fs a d\! | \clef "bass"

% bar 70
  d,,16-0\pp bf f bf d e! f c a c f a-0 |
  g16-4\< ef bf ef g a-0 bf-1 f-2 d f bf d |
  c16 a f a c d-1 ef-2 bf-4 g-1 bf ef g-4 | \break

% bar 73
  fs16\mf e! g fs e d-4 cs-3\> c-2 b-1 bf-1 a-0 fs-3 |
  b16\p g e d'-3 cs e d a-0 fs-1 d a'-0 fs-3 |
  b16 g e d'-2 cs e d a-0 fs-1 d a'-0 fs-3 | \break

% bar 76
  b16\> g e d'-2 cs e d fs-3 e d-4 cs b |
  e16-4 cs\! a-0\> fs-3 d'-4 b a-0 e a es a fs\! |
  b16-1 g e d'-2 cs e d a fs-1 d-0 a'-0 fs-3 | \break

% bar 79
  b16-1 g e d'-2 cs e d a fs-1 d-0 a'-0 fs-3 |
  c'16\< a fs d c a g b d g b d |
  g16-4 e-1 cs!-3 a-0 g-4 e d-0 fs-3
  a-0 \clef "tenor" d\thumb-0_II fs-2 a\thumb_"I" | \break

% bar 82
  c16-2\mf a\thumb-0 fs_"II" d c-3_III a
  g\thumb b-2 d\thumb_"II" g-3 b-1_I d-3 | \clef "treble"
  g16-2 fs-1 e\thumb-0 d-3_II cs b a\thumb b a g-3_III fs e |
  d16\thumb\mf\< fs-2 a\thumb_"II" d-3 fs-1_I a-3\!
  b-4 a-3\> fs-1 d-3_II a\thumb-0 fs-2_III | \break

% bar 85
  d16\thumb\< fs-2 a\thumb_"II" d-3 fs-1_I a-3\!
  b-4 a-3\> fs-1 d-3 a\thumb fs-2_III\! |
  d16\thumb-0 fs a\thumb-0_II d\thumb-0 fs-0-1 a-0-2
  cs-0-3 a-0-2 fs-0-1 d\thumb-0 a-0-2 fs-1 | \clef "bass"
  d16\thumb-0\< a-0_I fs-3 d-0 a-1 fs-4\!
  <d-1 a'-1>4.\f\fermata \bar "|." \pageBreak
% end
}

\score {
  \new Staff \eighteen
  \header {
    title = "18"
  }
  \layout {
  }
  \midi {
  }
}

% Local Variables:
% compile-command: "make -B 18.pdf"
% End:
