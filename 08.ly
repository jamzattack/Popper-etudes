\version "2.20"
\language "english"

\paper {
  print-all-headers = ##t
  tagline = ##f
}

eight = \relative c' {
  \tempo "Andante"
  \key c \major
  \time 4/4
  \clef "treble"

% bar 1
  c16\mf\thumb( d-1 e-2 g\thumb c-3 g a-1 e
  g c, e-2 gf,-1 g-1 c\thumb d e) |
  c16\thumb( d e g\thumb c-2 g a-1 e-2
  g c, e-2 fs,-1 g-4 c\thumb d e) | \break

% bar 3
  d16\thumb-0( e f a d-3 a bf-1 f
  a\thumb d,\thumb f gs,-1 a-1 d e f) |
  d16\thumb-0( e f a d-3 a bf-1 f
  a\thumb d,\thumb f gs,-1 a-1 d e f) | \break

% bar 5
  d16\thumb-0( e f b-1 d a b f
  a d, f a,-1 b-2 d e f) |
  d16\thumb-0( e f b-1 d a b f
  a d, f a,-1 b-2 d e f) | \break

% bar 7
  e16\thumb( g-2 a-3 c-1 e b\thumb c-1 g
  a-3 e g b,-1 c e\thumb a g) |
  e16\thumb( g-2 a-3 c-1 e b c g
  a-3 e g b,-1 c e\thumb a g) | \break

% bar 9
  fs16-1( ef'-3 b!\thumb c-1 cs-1 d b\thumb g
  e-1 df'-3 a\thumb bf-1 b-1 c a f |
  ds16-1) c'-3( gs\thumb a-1 as-1 b-2 gs e-2
  cs-1 bf'-3 fs\thumb g-1 gs-1 a-2 fs d | \break

% bar 11
  b16-1) af'-3( e\thumb f!-1 fs-1 g e c-2 \clef "bass"
  a-1 g'-4 e f af,-1 g'-4 e f) |
  g,16-1_>\<( f' ds e f,-1_> e'-4 cs d
  e,-1_> d' b c d,-0_> c'-4_D as-2 b-3) | \break

% bar 13
  c,16_G\f\thumb( d-1 e-2 g\thumb c-3 g\thumb a-1 e
  g\thumb c,\thumb e-2 fs,-1 g-1 c\thumb d e) |
  c16\thumb( d-1 e-2 g\thumb c-3 g a e
  g c, e fs,-1 g-1 c\thumb d e) | \break

% bar 15
  d16\thumb( e f a\thumb d-3 a bf-1 f
  a\thumb d,\thumb f-2 gs,-1 a-2 d\thumb e f) |
  d16\thumb( e f a\thumb d-3 a bf-1 f
  a d, f-2 gs,-1 a-1 d\thumb e f) | \break

% bar 17
  d16\thumb( e f b!-1 d-3 a\thumb b f
  a\thumb d,\thumb f-2 a,-1 b-2 d e f) |
  d16\thumb( e f b-1 d a\thumb b f
  a\thumb d,\thumb f-2 a,-1 b-2 d e f) | \pageBreak

% PAGE TWO

% bar 19
  e16\thumb( g-2 a-3 c-1 e-3 b\thumb c-1 g-2
  a e g b,-1 c-2 e\thumb a-3 g) |
  e16\thumb( g-2 a-3 c-1 e-3 b\thumb c-1 g-2
  a e g b,-1 c-2 e\thumb a-3 g) | \break

% bar 21
  fs16-1( ef'-3 b!\thumb c-1 cs-1 d-2 b\thumb g-2
  e-1 df'-3 a\thumb bf-1 b-1 c-2 a f-2 |
  ds-1) c'-3( gs\thumb a-1 as b-2 g e-2
  cs-1 bf'-3 fs\thumb g-1 gs a fs d-2 | \break

% bar 23
  b16-1) af'-3( e\thumb f-1 fs-1 g-2 e c-2
  a-1_> g'-4 e f af,-1_> g'-4 e f) |
  g,16-0_>\<( f'-4 ds e f,-1-> e'-4 cs d
  e,-1_> d'-4 b c d,-1_> c'-4 as-2 b-3) | \break

% bar 25
% swap phrasing slurs and real ones because of lilypond-mode's indenting.
  c,16\!(_\( d e g-0 c-1 d e g-4 c-4\)
  g\thumb_\( a-1 e-2 g_1\thumb c,_3\thumb e-2) g,-1\) |
  c,16_0(_\( d e g-0 c-4 d-0 e g-4 c-4\)
  g\thumb_\( a e g\thumb c,\thumb e-2 g,-1\)) | \break

% bar 27
  c,16( ef-1 fs-4 a-1 c-4 ef-1 fs-4 a-0
  c-2) ds-1( \clef "tenor" fs a-1 c-3 \clef "treble" ds-1 fs-3 a-0-3~ |
  a16)( g-2 e\thumb-0 b-1_II d c gs-1 a-2
  f'_I) e( c\thumb gs-1_II b a e-1 f | \break

% bar 29
  d'16-3_I) c( a\thumb-0 e-1_II g-3 f cs-1 d-2
  b'-3_I) a( f\thumb c-1 e-3 d-2 b-2 g\thumb) | \clef "bass"
  c!16-3( g\thumb a-1 e-2 f c\thumb d\thumb f-2
  d'-3) a\thumb( b-1 f-2 g d\thumb e\thumb g | \break

% bar 31
  e'-3) b\thumb( c-1 g-2 a-3 e\thumb f\thumb a-2\<
  f'-3) d-1( g\thumb e-2 \clef "tenor" a-1 f-3 e-2 g\thumb) | \clef "treble"
  c16\f-3( g\thumb a-1 e-2 f c\thumb d\thumb f-2
  d'-3) a\thumb( b-1 f-2 g-3 d\thumb e\thumb g-2 |
  e'16-3) b\thumb( c-1 g-2 a-3 e\thumb f\thumb a
  f'-3) cs-1( d-1 e-2 f-3 c-1 d-1 e-2) | \break

% bar 34
  f16( cs-1 d-1 e-2 f-3 c-1 d-1 af-2_II
  e'-3) b\thumb( c-1 d-2 ef-3-- b\thumb c-1 fs,-2_II |
  d'16-3\>) a!\thumb( bf c df-- a! bf e,_II
  c'-3) gs\thumb( a-1 d-2 c-3 g\thumb a-1 ef-2_II) | \break

% bar 36
  e16-2\p( g\thumb c b a\prall g\thumb f-3_II e \clef "bass"
  d-1_D c-4 b a g-4 fs-3 f-2 d-0) |
  c16-4\>( g-0 a-1 e g-0 c,-0 e-3 g
  c g a e g c, e g) |
  <c, e'-3>1\pp \bar "|." \pageBreak
% end
}

\score {
  \new Staff \eight
  \header {
    title = "8"
  }
  \layout {}
  \midi {}
}

% Local Variables:
% compile-command: "make -B 08.pdf" 
% End:
